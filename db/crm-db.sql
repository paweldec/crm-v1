-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Wersja serwera:               5.6.13-log - MySQL Community Server (GPL)
-- Serwer OS:                    Win64
-- HeidiSQL Wersja:              8.0.0.4396
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Zrzut struktury bazy danych crm-db
CREATE DATABASE IF NOT EXISTS `crm-db` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `crm-db`;


-- Zrzut struktury tabela crm-db.address
CREATE TABLE IF NOT EXISTS `address` (
  `addressId` int(11) NOT NULL AUTO_INCREMENT,
  `street` varchar(45) NOT NULL,
  `num` varchar(45) NOT NULL,
  `zipCode` varchar(6) NOT NULL,
  `city` int(11) DEFAULT NULL,
  `province` int(11) DEFAULT NULL,
  `coord_x` double NOT NULL,
  `coord_y` double NOT NULL,
  PRIMARY KEY (`addressId`),
  KEY `FK_province` (`province`),
  KEY `FK_city` (`city`),
  CONSTRAINT `FK_city` FOREIGN KEY (`city`) REFERENCES `city` (`cityId`),
  CONSTRAINT `FK_province` FOREIGN KEY (`province`) REFERENCES `province` (`provinceId`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- Dumping data for table crm-db.address: ~8 rows (około)
/*!40000 ALTER TABLE `address` DISABLE KEYS */;
REPLACE INTO `address` (`addressId`, `street`, `num`, `zipCode`, `city`, `province`, `coord_x`, `coord_y`) VALUES
	(1, 'Górczewska', '208', '01-460', 1, 7, 123.12312, 123.123123),
	(6, 'asd', '11', '11-111', 1, 7, 0, 0),
	(7, 'asd', '11', '11-111', 7, 7, 0, 0),
	(8, 'asd', '11', '11-111', 8, 9, 0, 0),
	(9, 'Krótka', '10', '01-444', 1, 7, 0, 0),
	(10, 'Grzybowska', '10', '01-440', 1, 6, 0, 0),
	(11, 'Długa 4', '21', '01-100', 1, 7, 0, 0),
	(12, 'Towarowa', '4', '01-440', 1, 7, 0, 0);
/*!40000 ALTER TABLE `address` ENABLE KEYS */;


-- Zrzut struktury tabela crm-db.city
CREATE TABLE IF NOT EXISTS `city` (
  `cityId` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT 'test',
  PRIMARY KEY (`cityId`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- Dumping data for table crm-db.city: ~3 rows (około)
/*!40000 ALTER TABLE `city` DISABLE KEYS */;
REPLACE INTO `city` (`cityId`, `name`) VALUES
	(1, 'Warszawa'),
	(7, 'Radom'),
	(8, 'Rzeszów');
/*!40000 ALTER TABLE `city` ENABLE KEYS */;


-- Zrzut struktury tabela crm-db.company
CREATE TABLE IF NOT EXISTS `company` (
  `companyId` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `NIP` varchar(45) DEFAULT NULL,
  `REGON` varchar(45) DEFAULT NULL,
  `address` int(11) DEFAULT NULL,
  `launch_year` year(4) DEFAULT NULL,
  PRIMARY KEY (`companyId`),
  KEY `FK_address` (`address`),
  CONSTRAINT `FK_address` FOREIGN KEY (`address`) REFERENCES `address` (`addressId`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Dumping data for table crm-db.company: ~5 rows (około)
/*!40000 ALTER TABLE `company` DISABLE KEYS */;
REPLACE INTO `company` (`companyId`, `name`, `NIP`, `REGON`, `address`, `launch_year`) VALUES
	(1, 'KolodkoBud', '1111111111', '1111111111', 1, '2013'),
	(2, 'Budimex', '2222222222', '2222222222', 9, '2007'),
	(3, 'Skanska', '3333333333', '3333333333', 10, '2009'),
	(4, 'BudMax', '4444444444', '4444444444', 11, '2001'),
	(5, 'ABC', '555555555', '555555555', 12, '1998');
/*!40000 ALTER TABLE `company` ENABLE KEYS */;


-- Zrzut struktury tabela crm-db.person
CREATE TABLE IF NOT EXISTS `person` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `lastname` varchar(45) DEFAULT NULL,
  `company` int(11) DEFAULT NULL,
  `login` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `is_admin` int(11) NOT NULL DEFAULT '0',
  `discriminator` varchar(45) NOT NULL DEFAULT 'P',
  PRIMARY KEY (`id`),
  KEY `FK_company` (`company`),
  CONSTRAINT `FK_company` FOREIGN KEY (`company`) REFERENCES `company` (`companyId`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8;

-- Dumping data for table crm-db.person: ~9 rows (około)
/*!40000 ALTER TABLE `person` DISABLE KEYS */;
REPLACE INTO `person` (`id`, `name`, `lastname`, `company`, `login`, `password`, `is_admin`, `discriminator`) VALUES
	(2, 'Paweł', 'Dec', NULL, 'admin', 'admin', 1, 'U'),
	(3, 'Andrzej', 'Strzelba', NULL, 'user', 'user', 0, 'U'),
	(41, 'imie', 'nazwisko', NULL, NULL, NULL, 0, 'P'),
	(42, 'imie', 'nazwisko', NULL, NULL, NULL, 0, 'P'),
	(46, 'Stanisław', 'Gutek', 3, NULL, NULL, 0, 'C'),
	(47, 'Mieczysław', 'Gutkowski', 2, NULL, NULL, 0, 'C'),
	(49, 'Andrzej', 'Strzelba', 4, NULL, NULL, 0, 'C'),
	(51, 'Jan', 'Kowalski', 1, NULL, NULL, 0, 'C'),
	(52, 'Zbyszek', 'Jakiś', 5, NULL, NULL, 0, 'C'),
	(53, 'Mieczysław', 'Ktoś', 1, NULL, NULL, 0, 'C');
/*!40000 ALTER TABLE `person` ENABLE KEYS */;


-- Zrzut struktury tabela crm-db.product
CREATE TABLE IF NOT EXISTS `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `amount` int(10) NOT NULL,
  `price` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- Dumping data for table crm-db.product: ~5 rows (około)
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
REPLACE INTO `product` (`id`, `name`, `amount`, `price`) VALUES
	(2, 'Pędzel', 29, 5.5),
	(4, 'Farba zielona 1l', 39, 34.4),
	(5, 'Łopata', 10, 34.5),
	(6, 'Młotek', 48, 28),
	(7, 'Rękawice', 30, 10);
/*!40000 ALTER TABLE `product` ENABLE KEYS */;


-- Zrzut struktury tabela crm-db.province
CREATE TABLE IF NOT EXISTS `province` (
  `provinceId` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`provinceId`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- Dumping data for table crm-db.province: ~16 rows (około)
/*!40000 ALTER TABLE `province` DISABLE KEYS */;
REPLACE INTO `province` (`provinceId`, `name`) VALUES
	(1, 'dolnośląskie'),
	(2, 'kujawsko-pomorskie'),
	(3, 'lubelskie'),
	(4, 'lubuskie'),
	(5, 'łódzkie'),
	(6, 'małopolskie'),
	(7, 'mazowieckie'),
	(8, 'opolskie'),
	(9, 'podkarpackie'),
	(10, 'podlaskie'),
	(11, 'pomorskie'),
	(12, 'śląskie'),
	(13, 'świętokrzyskie'),
	(14, 'warmińsko-mazurskie'),
	(15, 'wielkopolskie'),
	(16, 'zachodniopomorskie');
/*!40000 ALTER TABLE `province` ENABLE KEYS */;


-- Zrzut struktury tabela crm-db.purchase
CREATE TABLE IF NOT EXISTS `purchase` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer` int(11) NOT NULL,
  `status` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_customer2` (`customer`),
  CONSTRAINT `FK_customer2` FOREIGN KEY (`customer`) REFERENCES `person` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

-- Dumping data for table crm-db.purchase: ~9 rows (około)
/*!40000 ALTER TABLE `purchase` DISABLE KEYS */;
REPLACE INTO `purchase` (`id`, `customer`, `status`) VALUES
	(24, 47, 1),
	(25, 47, 1),
	(26, 47, 1),
	(27, 47, 0),
	(28, 47, 0),
	(29, 49, 1),
	(30, 49, 1),
	(31, 51, 1),
	(32, 52, 1),
	(33, 53, 1);
/*!40000 ALTER TABLE `purchase` ENABLE KEYS */;


-- Zrzut struktury tabela crm-db.purchase_item
CREATE TABLE IF NOT EXISTS `purchase_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product` int(11) NOT NULL,
  `purchase` int(11) NOT NULL,
  `amount` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_product` (`product`),
  KEY `FK_purchase` (`purchase`),
  CONSTRAINT `FK_product` FOREIGN KEY (`product`) REFERENCES `product` (`id`),
  CONSTRAINT `FK_purchase` FOREIGN KEY (`purchase`) REFERENCES `purchase` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8;

-- Dumping data for table crm-db.purchase_item: ~16 rows (około)
/*!40000 ALTER TABLE `purchase_item` DISABLE KEYS */;
REPLACE INTO `purchase_item` (`id`, `product`, `purchase`, `amount`) VALUES
	(47, 2, 24, 2),
	(48, 4, 24, 4),
	(49, 4, 25, 12),
	(50, 2, 25, 12),
	(51, 4, 26, 2),
	(52, 2, 26, 1),
	(53, 4, 27, 5),
	(54, 2, 27, 4),
	(55, 2, 28, 8),
	(56, 4, 28, 9),
	(57, 2, 29, 10),
	(58, 4, 29, 5),
	(59, 2, 30, 10),
	(60, 4, 30, 5),
	(61, 2, 31, 19),
	(62, 4, 31, 10),
	(63, 4, 32, 5),
	(64, 2, 32, 10),
	(65, 6, 33, 2),
	(66, 2, 33, 10);
/*!40000 ALTER TABLE `purchase_item` ENABLE KEYS */;


-- Zrzut struktury tabela crm-db.user_customers
CREATE TABLE IF NOT EXISTS `user_customers` (
  `user` int(11) NOT NULL DEFAULT '0',
  `customer` int(11) NOT NULL DEFAULT '0',
  KEY `FK_user` (`user`),
  KEY `FK_customer` (`customer`),
  CONSTRAINT `FK_customer` FOREIGN KEY (`customer`) REFERENCES `person` (`id`),
  CONSTRAINT `FK_user` FOREIGN KEY (`user`) REFERENCES `person` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table crm-db.user_customers: ~1 rows (około)
/*!40000 ALTER TABLE `user_customers` DISABLE KEYS */;
REPLACE INTO `user_customers` (`user`, `customer`) VALUES
	(2, 53);
/*!40000 ALTER TABLE `user_customers` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
