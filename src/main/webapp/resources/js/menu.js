$(document).ready(function(){ 
  var menuShowPanel = function(panel) {
	  var menuPanel = null;
	  
	  switch(panel) {
	  	  case "general":{
	  		menuPanel = $('.ui-panelmenu-panel').filter('.general');
	  		break;
	  	  }
		  case "clients":{
			  menuPanel = $('.ui-panelmenu-panel').filter('.clients');
			  break;
		  }
		  case "products": {
			  menuPanel = $('.ui-panelmenu-panel').filter('.products');
			  break;
		  }
		  case "transactions": {
			  menuPanel = $('.ui-panelmenu-panel').filter('.transactions');
			  break;
		  }
		  case "reports": {
			  menuPanel = $('.ui-panelmenu-panel').filter('.reports');
			  break;
		  }
		  case "management": {
			  menuPanel = $('.ui-panelmenu-panel').filter('.management');
			  break;
		  }
		  default: {
			  console.log("cannot find menu panel for: " + panel);
			  return;
		  }
	  }
	  
	  menuPanel.find(".ui-panelmenu-content").css("display","block");
	  menuPanel.find(".ui-panelmenu-header").addClass("ui-state-active"); 
	  menuPanel.find(".ui-icon-triangle-1-e").removeClass("ui-icon-triangle-1-e").addClass("ui-icon-triangle-1-s"); 
  };
  
  var url = $(location).attr('href');
  if((url.indexOf("/platform/modules/announcements/") != -1) || url.indexOf("/platform/modules/schedule/") != -1) {
	  menuShowPanel("general");
  }
  else if((url.indexOf("/platform/modules/agents/") != -1) || url.indexOf("/platform/modules/contractors/") != -1) {
	  menuShowPanel("clients");
  }
  else if(url.indexOf("/platform/modules/products/") != -1) {
	  menuShowPanel("products");
  }
  else if(url.indexOf("/platform/modules/transactions/") != -1) {
	  menuShowPanel("transactions");
  }
  else if(url.indexOf("/platform/modules/reports/") != -1) {
	  menuShowPanel("reports");
  }
  else if(url.indexOf("/platform/modules/users/") != -1) {
	  menuShowPanel("management");
  }
});



