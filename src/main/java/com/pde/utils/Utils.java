package com.pde.utils;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

@Named("utils")
@RequestScoped
public class Utils
{
	public boolean isModule(String module, String url)
	{
		String urlToCheck = "/platform/modules/" + module + "/";
		
		if(url.contains(urlToCheck))
		{
			return true;
		}
		
		return false;
	}
	
	public String url(String module, String view)
	{
		return "/platform/modules/" + module + "/" + view + ".xhtml";
	}

	public String url(String module, String view, boolean redirect)
	{
		String url = this.url(module, view);

		if (redirect)
		{
			url += "?faces-redirect=true";
		}

		return url;
	}

	public String url(String module, String view, boolean redirect,
			UrlParam... params)
	{
		String url = this.url(module, view, redirect);

		if (params == null)
		{
			return url;
		}

		for (int i = 0; i < params.length; i++)
		{
			url += "&" + params[i].getKey() + "=" + params[i].getValue();
		}

		return url;
	}

	public static String convertToUTF8(String s)
	{
		String out = null;
		
		try
		{
			out = new String(s.getBytes("UTF-8"), "ISO-8859-1");
		} 
		catch (java.io.UnsupportedEncodingException e)
		{
			return null;
		}
		
		return out;
	}
	
	public <T> List<T> convertSetToList(Set<T> set)
	{
		List<T> list = new ArrayList<T>();
		list.addAll(set);
		return list;
	}
	
	public <T extends Comparable<T>> List<T> convertSetToListAndSort(Set<T> set)
	{
		List<T> list = this.convertSetToList(set);
		Collections.sort(list);
		return list;
	}
	
	public String formatCurrency(double value)
	{
		DecimalFormat df = new DecimalFormat("#0.00");
		return df.format(value);
	}
	
	public String formatDate(Date date)
	{
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		return df.format(date);
	}
	
	public String md5(String value)
	{		
		String result = value;
		
		try
		{
		    if(value != null) 
		    {
		        MessageDigest md = MessageDigest.getInstance("MD5"); 
		        md.update(value.getBytes());
		        BigInteger hash = new BigInteger(1, md.digest());
		        result = hash.toString(16);
		        
		        while(result.length() < 32) 
		        { 
		            result = "0" + result;
		        }
		    }
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
			return null;
		}
		
	    return result;
	}
}
