package com.pde.utils;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;  

public class FacesLogger
{	
	private FacesLogger() {}
	
	public static void info(String message)
	{
		FacesMessage msg = new FacesMessage(message, "Informacja");
		msg.setSeverity(FacesMessage.SEVERITY_INFO);
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}
	
	public static void error(String message)
	{
		FacesMessage msg = new FacesMessage(message, "Błąd");
		msg.setSeverity(FacesMessage.SEVERITY_ERROR);
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}
	
	public static void warn(String message)
	{
		FacesMessage msg = new FacesMessage(message, "Ostrzeżenie");
		msg.setSeverity(FacesMessage.SEVERITY_WARN);
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}
}
