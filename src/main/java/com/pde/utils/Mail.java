package com.pde.utils;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

@Stateless
public class Mail
{
	@Resource(name = "java:jboss/mail/gmail")
    private Session session;
	
	public boolean send(String addresses, String topic, String content)
	{
		try
		{
			Message message = new MimeMessage(session);
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(addresses));
			message.setSubject(topic);
			message.setText(content);
			
			Transport.send(message);
		}
		catch(MessagingException e)
		{
			System.out.println(e.getMessage());
			return false;
		}
		
		return true;
	}
}
