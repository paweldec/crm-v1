package com.pde.utils;

public class UrlParam
{
	private String key;
	private String value;
	
	public UrlParam(String key, String value)
	{
		this.key = key;
		this.value = value;
	}
	
	public String getKey()
	{
		return key;
	}
	
	public String getValue()
	{
		return value;
	}
}
