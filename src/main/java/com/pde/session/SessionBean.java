package com.pde.session;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

@Named("sessionBean")
@SessionScoped
public class SessionBean implements Serializable
{
	private static final long serialVersionUID = 5873457096373124257L;

	private long userId;
	private boolean loggedIn;
	private String username;
	private boolean isAdmin;

	public SessionBean()
	{
		userId = -1;
		loggedIn = false;
		username = "anonim";
		isAdmin = false;
	}
	
	public boolean isLoggedIn()
	{
		return loggedIn;
	}

	public void setLoggedIn(boolean loggedIn)
	{
		this.loggedIn = loggedIn;
	}

	public String getUsername()
	{
		return username;
	}

	public void setUsername(String username)
	{
		this.username = username;
	}

	public long getUserId()
	{
		return userId;
	}

	public void setUserId(long userId)
	{
		this.userId = userId;
	}

	public boolean isAdmin()
	{
		return isAdmin;
	}

	public void setAdmin(boolean isAdmin)
	{
		this.isAdmin = isAdmin;
	}
}
