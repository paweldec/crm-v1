package com.pde.converters;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.pde.models.Trade;
import com.pde.services.TradeService;

@ManagedBean
@FacesConverter("TradeConverter")
public class TradeConverter implements Converter
{
	@EJB(name="TradeService")
	private TradeService tradeService;
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String str)
	{
		Trade trade = tradeService.getTradeByName(str);
		
		if(trade == null) 
		{
			trade = new Trade();
			trade.setName(str);
		}
		
		return trade;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object object)
	{
		if(object instanceof Trade)
		{
			Trade trade = (Trade)object;
			return trade.getName();
		}
		
		return null;
	}	
}
