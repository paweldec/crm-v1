package com.pde.converters;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.pde.models.Contractor;
import com.pde.services.ContractorService;

@ManagedBean
@FacesConverter("ContractorConverter")
public class ContractorConverter implements Converter
{
	@EJB(name = "ContractorService")
	private ContractorService contractorService;
	
	@Override
	public Object getAsObject(FacesContext facesContext, UIComponent component, String str)
	{
		Contractor contractor = contractorService.getContractorByName(str);
		return contractor;
	}

	@Override
	public String getAsString(FacesContext facesContext, UIComponent component, Object object)
	{
		if(object instanceof Contractor) 
		{
			Contractor contractor = (Contractor)object;
			return contractor.getName();
		}
		
		return null;
	}

}
