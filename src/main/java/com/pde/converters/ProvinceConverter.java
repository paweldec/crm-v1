package com.pde.converters;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.pde.models.Province;
import com.pde.services.ProvinceService;

@ManagedBean
@FacesConverter("ProvinceConverter")
public class ProvinceConverter implements Converter
{
	@EJB(name = "ProvinceService")
	private ProvinceService provinceService;
	
	@Override
	public Object getAsObject(FacesContext facesContext, UIComponent compontent, String str)
	{		
		Province province = provinceService.getProvinceByName(str);

		if(province == null) 
		{
			province = new Province();
			province.setName(str);
		}
		
		return province;
	}

	@Override
	public String getAsString(FacesContext facesContext, UIComponent component, Object object)
	{
		if(object instanceof Province)
		{
			Province province = (Province)object;
			return province.getName();
		}
		
		return null;
	}
}
