package com.pde.converters;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@ManagedBean
@FacesConverter(forClass=java.util.Date.class)
public class DateConverter implements Converter
{	
	@Override
	public Object getAsObject(FacesContext facesContext, UIComponent component, String str)
	{
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		Date date;
		
		try
		{
			date = df.parse(str);
		} 
		catch (ParseException e)
		{
			System.out.println(e.getMessage());
			return null;
		}
		
		return date;
	}

	@Override
	public String getAsString(FacesContext facesContext, UIComponent component, Object object)
	{
		if(!(object instanceof Date))
		{
			return "";
		}
		
		Date date = (Date)object;
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		return df.format(date);
	}

}