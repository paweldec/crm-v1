package com.pde.converters;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.pde.models.Activity;
import com.pde.services.ActivityService;

@ManagedBean
@FacesConverter("ActivityConverter")
public class ActivityConverter implements Converter
{
	@EJB(name="ActivityService")
	private ActivityService activityService;
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String str)
	{
		Activity activity = activityService.getActivityByName(str);
		
		if(activity == null) 
		{
			activity = new Activity();
			activity.setName(str);
		}
		
		return activity;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object object)
	{
		if(object instanceof Activity)
		{
			Activity activity = (Activity)object;
			return activity.getName();
		}
		
		return null;
	}	
}
