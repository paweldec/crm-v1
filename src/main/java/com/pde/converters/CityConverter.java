package com.pde.converters;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.pde.models.City;
import com.pde.services.CityService;

@ManagedBean
@FacesConverter("CityConverter")
public class CityConverter implements Converter
{
	@EJB(name = "CityService")
	private CityService cityService;
	
	@Override
	public Object getAsObject(FacesContext facesContext, UIComponent component, String str)
	{			
		City city = cityService.getCityByName(str);		
		
		if(city == null) 
		{
			city = new City();
			city.setName(str);
		}
		
		return city;
	}

	@Override
	public String getAsString(FacesContext facesContext, UIComponent component, Object object)
	{
		if(object instanceof City) 
		{
			City city = (City)object;
			return city.getName();
		}
		
		return null;
	}

}
