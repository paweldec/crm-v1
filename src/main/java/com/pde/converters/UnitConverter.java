package com.pde.converters;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.pde.models.Unit;
import com.pde.services.UnitService;

@ManagedBean
@FacesConverter("UnitConverter")
public class UnitConverter implements Converter
{
	@EJB(name="UnitService")
	private UnitService unitService;
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String str)
	{
		Unit unit = unitService.getUnitByName(str);
		
		if(unit == null) 
		{
			unit = new Unit();
			unit.setName(str);
		}
		
		return unit;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object object)
	{
		if(object instanceof Unit)
		{
			Unit unit = (Unit)object;
			return unit.getName();
		}
		
		return null;
	}	
}
