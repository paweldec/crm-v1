package com.pde.converters;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.pde.models.Country;
import com.pde.services.CountryService;

@ManagedBean
@FacesConverter("CountryConverter")
public class CountryConverter implements Converter
{
	@EJB(name = "CountryService")
	private CountryService countryService;
	
	@Override
	public Object getAsObject(FacesContext facesContext, UIComponent component, String str)
	{
		Country country = countryService.getCountryByName(str);
		
		if(country == null)
		{
			country = new Country();
			country.setName(str);
		}
		
		return country;
	}

	@Override
	public String getAsString(FacesContext facesContext, UIComponent component, Object object)
	{
		if(object instanceof Country) 
		{
			Country country = (Country)object;
			return country.getName();
		}
		
		return null;
	}
}
