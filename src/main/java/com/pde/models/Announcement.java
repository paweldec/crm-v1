package com.pde.models;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@NamedQueries(
{ 
	@NamedQuery(name = "Announcement.findAll", query = "SELECT a FROM Announcement a"),
	@NamedQuery(name = "Announcement.findAcutalAndSortedByDate", query = "SELECT a FROM Announcement a "
			+ "WHERE a.toDate > CURRENT_TIMESTAMP ORDER BY a.date DESC")
})
public class Announcement implements Serializable
{
	private static final long serialVersionUID = -7332004328901401515L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(nullable = false, length = 5000)
	private String content;
	
	@ManyToOne
	@JoinColumn(name="user", referencedColumnName="id")
	private User user;
	
	@Column(nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date date;
	
	@Column(nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date toDate;
	
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy="announcement")
	private List<AnnouncementAnswer> announcementAnswers;

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public String getContent()
	{
		return content;
	}

	public void setContent(String content)
	{
		this.content = content;
	}

	public User getUser()
	{
		return user;
	}

	public void setUser(User user)
	{
		this.user = user;
	}

	public Date getDate()
	{
		return date;
	}

	public void setDate(Date date)
	{
		this.date = date;
	}

	public List<AnnouncementAnswer> getAnnouncementAnswers()
	{
		return announcementAnswers;
	}

	public void setAnnouncementAnswers(List<AnnouncementAnswer> announcementAnswers)
	{
		this.announcementAnswers = announcementAnswers;
	}

	public Date getToDate()
	{
		return toDate;
	}

	public void setToDate(Date toDate)
	{
		this.toDate = toDate;
	}
}
