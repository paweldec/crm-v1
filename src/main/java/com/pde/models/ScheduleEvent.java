package com.pde.models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="schedule_event")
@NamedQueries(
{ 
	@NamedQuery(name = "ScheduleEvent.findAll", query = "SELECT se FROM ScheduleEvent se"),
	@NamedQuery(name = "ScheduleEvent.findByUser", query = "SELECT se FROM ScheduleEvent se "
			+ "WHERE se.user = :user"),
	@NamedQuery(name = "ScheduleEvent.findByEventId", query = "SELECT se FROM ScheduleEvent se "
			+ "WHERE se.eventId = :eventId")
})
public class ScheduleEvent implements Serializable
{
	private static final long serialVersionUID = 1563904237436422124L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(nullable = false, length = 200)
	private String name;
	
	@Column(nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date startDate;
	
	@Column(nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date endDate;
	
	@ManyToOne
	@JoinColumn(name="user", referencedColumnName="id")
	private User user;
	
	@Column(nullable = false)
	private String eventId;

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public Date getStartDate()
	{
		return startDate;
	}

	public void setStartDate(Date startDate)
	{
		this.startDate = startDate;
	}

	public Date getEndDate()
	{
		return endDate;
	}

	public void setEndDate(Date endDate)
	{
		this.endDate = endDate;
	}

	public User getUser()
	{
		return user;
	}

	public void setUser(User user)
	{
		this.user = user;
	}

	public String getEventId()
	{
		return eventId;
	}

	public void setEventId(String eventId)
	{
		this.eventId = eventId;
	}
}
