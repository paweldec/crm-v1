package com.pde.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@NamedQueries(
{ 
	@NamedQuery(name = "Product.findAll", query = "SELECT p FROM Product p ORDER BY p.name"),
	@NamedQuery(name = "Product.findByName", query = "SELECT p FROM Product p "
			+ "WHERE p.name = :name")
})
public class Product implements Serializable
{
	private static final long serialVersionUID = 3141252507593272837L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(nullable = false, length = 50)
	private String name;
	
	@Column(nullable = false)
	private int amount;
	
	@ManyToOne
	@JoinColumn(name="unit", referencedColumnName="id")
	private Unit unit;
	
	@Column(nullable = true, length = 100)
	private String description;
	
	@Column(nullable = false)
	private double price; 

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public int getAmount()
	{
		return amount;
	}

	public void setAmount(int amount)
	{
		this.amount = amount;
	}

	public Unit getUnit()
	{
		return unit;
	}

	public void setUnit(Unit unit)
	{
		this.unit = unit;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}
	
	public double getPrice()
	{
		return price;
	}

	public void setPrice(double price)
	{
		this.price = price;
	}

	@Override
	public boolean equals(Object obj)
	{
		return (obj instanceof Product) && id == (((Product)obj).id);
	}
}
