package com.pde.models;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

@Entity
@NamedQueries(
{ 
	@NamedQuery(name = "Province.findAll", query = "SELECT p FROM Province p"),
	@NamedQuery(name = "Province.findByName", query = "SELECT p FROM Province p "
			+ "WHERE p.name = :name")
})
public class Province implements Serializable
{
	private static final long serialVersionUID = 1250559059928629933L;
		
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(nullable = false, length = 20)
	private String name;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "province", orphanRemoval = true)
	private List<Contractor> contractors;

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public List<Contractor> getContractors()
	{
		return contractors;
	}

	public void setContractors(List<Contractor> contractors)
	{
		this.contractors = contractors;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		return (obj instanceof Province) && id == (((Province)obj).id);
	}
}
