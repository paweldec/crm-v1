package com.pde.models;

public enum Visibility {
    HIDDEN,
    SHOWN
}