package com.pde.models;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@NamedQueries(
{ 
	@NamedQuery(name = "User.findAll", query = "SELECT u FROM User u "
			+ "WHERE u.visibility = com.pde.models.Visibility.SHOWN"),
	@NamedQuery(name = "User.findByLoginAndPassword", query = "SELECT u FROM User u "
			+ "WHERE u.login = :login AND u.password = :password "
			+ "AND u.visibility = com.pde.models.Visibility.SHOWN"),
	@NamedQuery(name = "User.findByLogin", query = "SELECT u FROM User u "
			+ "WHERE u.login = :login "
			+ "AND u.visibility = com.pde.models.Visibility.SHOWN")
})
public class User implements Serializable
{
	private static final long serialVersionUID = -401502525405694684L;
	
	public enum Role 
	{
		USER,
		ADMIN
	}
		
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(unique = true, nullable = false, length = 10)
	private String login;
	
	@Column(nullable = false, length = 100) // md5
	private String password;
	
	@Column(nullable = false)
	@Enumerated(EnumType.STRING)
	private Role role; 
	
	@Column(nullable = false)
	@Enumerated(EnumType.STRING)
    private Visibility visibility;
	
	public User()
	{
		visibility = Visibility.SHOWN;
	}

	public long getId()
	{
		return this.id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public String getLogin()
	{
		return this.login;
	}

	public void setLogin(String login)
	{
		this.login = login;
	}

	public String getPassword()
	{
		return this.password;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}
	
	public Role getRole()
	{
		return role;
	}

	public void setRole(Role role)
	{
		this.role = role;
	}

	public Visibility getVisibility()
	{
		return visibility;
	}

	public void setVisibility(Visibility visibility)
	{
		this.visibility = visibility;
	}
}