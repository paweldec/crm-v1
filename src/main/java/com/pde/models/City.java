package com.pde.models;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

@Entity
@NamedQueries(
{ 
	@NamedQuery(name = "City.findAll", query = "SELECT c FROM City c"),
	@NamedQuery(name = "City.findByNameLike", query = "SELECT c FROM City c "
			+ "WHERE UPPER(c.name) LIKE :name"),
	@NamedQuery(name = "City.findByName", query = "SELECT c FROM City c "
			+ "WHERE c.name = :name")
	
})
public class City implements Serializable
{
	private static final long serialVersionUID = -3932357957645932114L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(nullable = false, length = 50)
	private String name;

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}
}
