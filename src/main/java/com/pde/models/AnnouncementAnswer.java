package com.pde.models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="announcement_answer")
@NamedQueries(
{ 
	@NamedQuery(name = "AnnouncementAnswer.findAll", query = "SELECT a FROM AnnouncementAnswer a "),
})
public class AnnouncementAnswer implements Serializable
{
	private static final long serialVersionUID = -1785006813350834257L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(nullable = false, length = 5000)
	private String content;
	
	@ManyToOne
	@JoinColumn(name="user", referencedColumnName="id")
	private User user;
	
	@ManyToOne
	@JoinColumn(name="announcement", referencedColumnName="id")
	private Announcement announcement;
	
	@Column(nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date date;

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public String getContent()
	{
		return content;
	}

	public void setContent(String content)
	{
		this.content = content;
	}

	public User getUser()
	{
		return user;
	}

	public void setUser(User user)
	{
		this.user = user;
	}

	public Announcement getAnnouncement()
	{
		return announcement;
	}

	public void setAnnouncement(Announcement announcement)
	{
		this.announcement = announcement;
	}

	public Date getDate()
	{
		return date;
	}

	public void setDate(Date date)
	{
		this.date = date;
	}
}
