package com.pde.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@NamedQueries(
{ 
	@NamedQuery(name = "Unit.findAll", query = "SELECT u FROM Unit u"),
	@NamedQuery(name = "Unit.findByName", query = "SELECT u FROM Unit u "
			+ "WHERE u.name = :name")
})
public class Unit implements Serializable
{
	private static final long serialVersionUID = -3360437303107623298L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(nullable = false, length = 10)
	private String name;

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}	
	
	@Override
	public boolean equals(Object obj)
	{
		return (obj instanceof Unit) && id == (((Unit)obj).id);
	}
}
