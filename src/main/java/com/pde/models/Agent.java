package com.pde.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@NamedQueries(
{ 
	@NamedQuery(name = "Agent.findAll", query = "SELECT a FROM Agent a "
			+ "WHERE a.visibility = com.pde.models.Visibility.SHOWN"),
})
public class Agent implements Serializable
{
	private static final long serialVersionUID = 4572283583536566963L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(nullable = false, length = 20)
	private String name;
	
	@Column(nullable = false, length = 20)
	private String lastName;
	
	@Column(nullable = true, length = 20)
	private String phoneNumber;
	
	@Column(nullable = true, length = 50)
	private String email;
	
	@ManyToMany(fetch = FetchType.EAGER, mappedBy = "agents")
	private List<Contractor> contractors = new ArrayList<Contractor>(0);
	
	@Column(nullable = false)
	@Enumerated(EnumType.STRING)
    private Visibility visibility;
	
	public Agent()
	{
		visibility = Visibility.SHOWN;
	}
	
	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}

	public String getPhoneNumber()
	{
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber)
	{
		this.phoneNumber = phoneNumber;
		
		if(this.phoneNumber != null && this.phoneNumber.length() == 0)
		{
			this.phoneNumber = null;
		}
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
		
		if(this.email != null && this.email.length() == 0)
		{
			this.email = null;
		}
	}

	public List<Contractor> getContractors()
	{
		return contractors;
	}

	public void setContractors(List<Contractor> contractors)
	{
		this.contractors = contractors;
	}
	
	public Visibility getVisibility()
	{
		return visibility;
	}

	public void setVisibility(Visibility visibility)
	{
		this.visibility = visibility;
	}

	@Override
	public boolean equals(Object obj)
	{
		return (obj instanceof Agent) && id == (((Agent)obj).id);
	}
}
