package com.pde.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name="transaction_product")
@NamedQueries(
{ 
	@NamedQuery(name = "TransactionProduct.findAll", query = "SELECT t FROM TransactionProduct t")
})
public class TransactionProduct implements Serializable
{
	private static final long serialVersionUID = 844398444603252291L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@ManyToOne
	@JoinColumn(name="transaction", referencedColumnName="id")
	private Transaction transaction;
	
	@ManyToOne
	@JoinColumn(name="product", referencedColumnName="id")
	private Product product;
	
	@Column(nullable = false)
	private int amount;

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public Transaction getTransaction()
	{
		return transaction;
	}

	public void setTransaction(Transaction transaction)
	{
		this.transaction = transaction;
	}

	public Product getProduct()
	{
		return product;
	}

	public void setProduct(Product product)
	{
		this.product = product;
	}

	public int getAmount()
	{
		return amount;
	}

	public void setAmount(int amount)
	{
		this.amount = amount;
	}
}
