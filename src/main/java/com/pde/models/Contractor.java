package com.pde.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

@Entity
@NamedQueries(
{ 
	@NamedQuery(name = "Contractor.findAll", query = "SELECT c FROM Contractor c "
			+ "WHERE c.visibility = com.pde.models.Visibility.SHOWN"),
	@NamedQuery(name = "Contractor.findByUnique", query = "SELECT c FROM Contractor c "
			+ "WHERE c.name = :name AND c.NIP = :NIP AND c.REGON = :REGON "
			+ "AND c.PESEL = :PESEL AND c.visibility = com.pde.models.Visibility.SHOWN"),
	@NamedQuery(name = "Contractor.findByNameLike", query = "SELECT c FROM Contractor c "
			+ "WHERE UPPER(c.name) LIKE :name AND c.visibility = com.pde.models.Visibility.SHOWN"),
	@NamedQuery(name = "Contractor.findByName", query = "SELECT c FROM Contractor c "
			+ "WHERE c.name = :name AND c.visibility = com.pde.models.Visibility.SHOWN")
})
public class Contractor implements Serializable
{
	private static final long serialVersionUID = 652612083580281015L;
	
	public enum ContractorType {
	    COMPANY,
	    PERSON
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(nullable = false, length = 50)
	private String name;
	
	@Column(nullable = false, length = 13)
	private String NIP;
	
	@Column(nullable = true, length = 14)
	private String REGON;
	
	@Column(nullable = true, length = 11)
	private String PESEL;
	
	@Column(nullable = true, length = 50)
	private String street;
	
	@Column(nullable = false, length = 10)
	private String num;
	
	@Column(nullable = false, length = 6)
	private String zipCode;
	
	@Column(nullable = true, length = 50)
	private String description;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy="contractor")
	private List<Transaction> transactions;
	
	@Column(nullable = false)
	@Enumerated(EnumType.STRING)
    private ContractorType type;
	
	@ManyToOne
	@JoinColumn(name="city", referencedColumnName="id")
	private City city;
	
	@ManyToOne
	@JoinColumn(name="province", referencedColumnName="id")
	private Province province;
	
	@ManyToOne
	@JoinColumn(name="country", referencedColumnName="id")
	private Country country;
	
	@ManyToOne
	@JoinColumn(name="trade", referencedColumnName="id")
	private Trade trade;
	
	@ManyToOne
	@JoinColumn(name="activity", referencedColumnName="id")
	private Activity activity;
	
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "contractor_agent", joinColumns = { 
			@JoinColumn(name = "contractor", nullable = false, updatable = false) }, 
			inverseJoinColumns = { @JoinColumn(name = "agent", 
					nullable = false, updatable = false) })
	private List<Agent> agents = new ArrayList<Agent>(0);
	
	@Column(nullable = false)
	@Enumerated(EnumType.STRING)
    private Visibility visibility;

	public Contractor()
	{
		visibility = Visibility.SHOWN;
	}
	
	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getNIP()
	{
		return NIP;
	}

	public void setNIP(String NIP)
	{
		this.NIP = NIP;
	}

	public String getREGON()
	{
		return REGON;
	}

	public void setREGON(String REGON)
	{
		this.REGON = REGON;
		
		if(this.REGON != null && this.REGON.length() == 0)
		{
			this.REGON = null;
		}
	}

	public String getPESEL()
	{
		return PESEL;
	}

	public void setPESEL(String PESEL)
	{
		this.PESEL = PESEL;
	
		if(this.PESEL != null && this.PESEL.length() == 0)
		{
			this.PESEL = null;
		}
	}
	
	public String getStreet()
	{
		return street;
	}

	public void setStreet(String street)
	{
		this.street = street;
	}

	public String getNum()
	{
		return num;
	}

	public void setNum(String num)
	{
		this.num = num;
	}

	public String getZipCode()
	{
		return zipCode;
	}

	public void setZipCode(String zipCode)
	{
		this.zipCode = zipCode;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public City getCity()
	{
		return city;
	}

	public void setCity(City city)
	{
		this.city = city;
	}

	public Province getProvince()
	{
		return province;
	}

	public void setProvince(Province province)
	{
		this.province = province;
	}

	public Country getCountry()
	{
		return country;
	}

	public void setCountry(Country country)
	{
		this.country = country;
	}

	public Trade getTrade()
	{
		return trade;
	}

	public void setTrade(Trade trade)
	{
		this.trade = trade;
	}

	public Activity getActivity()
	{
		return activity;
	}

	public void setActivity(Activity activity)
	{
		this.activity = activity;
	}

	public ContractorType getType()
	{
		return type;
	}

	public void setType(ContractorType type)
	{
		this.type = type;
	}

	public List<Agent> getAgents()
	{
		return agents;
	}

	public void setAgents(List<Agent> agents)
	{
		this.agents = agents;
	}
	
	public List<Transaction> getTransactions()
	{
		return transactions;
	}

	public void setTransactions(List<Transaction> transactions)
	{
		this.transactions = transactions;
	}

	public Visibility getVisibility()
	{
		return visibility;
	}

	public void setVisibility(Visibility visibility)
	{
		this.visibility = visibility;
	}

	@Override
	public boolean equals(Object obj)
	{
		return (obj instanceof Contractor) && id == (((Contractor)obj).id);
	}
}
