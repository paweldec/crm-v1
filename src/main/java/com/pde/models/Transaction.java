package com.pde.models;

import com.pde.models.TransactionProduct;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@NamedQueries(
{ 
	@NamedQuery(name = "Transaction.findAll", query = "SELECT t FROM Transaction t"),
	@NamedQuery(name = "Transaction.findAllBetweenDate", query = "SELECT t FROM Transaction t "
			+ "WHERE t.date BETWEEN :start AND :end"),
	@NamedQuery(name = "Transaction.findTransactionsByStatus", query = "SELECT t FROM Transaction t "
			+ "WHERE t.status = :status ORDER BY t.id DESC"),
	@NamedQuery(name = "Transaction.findById", query = "SELECT t FROM Transaction t "
			+ "WHERE t.id = :id")
})
public class Transaction implements Serializable
{
	private static final long serialVersionUID = 5233367726802246162L;
	
	public enum Status
	{
		WAITING,
		COMPLETE			
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@ManyToOne
	@JoinColumn(name="contractor", referencedColumnName="id")
	private Contractor contractor;
	
	@ManyToOne
	@JoinColumn(name="agent", referencedColumnName="id")
	private Agent agent;
	
	@Column(nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date date;
	
	@Column(nullable = false)
	@Enumerated(EnumType.STRING)
	private Status status;
	
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy="transaction")
	private Set<TransactionProduct> transactionProducts;
	
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy="transaction")
	private Set<TransactionComment> comments;

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public Contractor getContractor()
	{
		return contractor;
	}

	public void setContractor(Contractor contractor)
	{
		this.contractor = contractor;
	}

	public Agent getAgent()
	{
		return agent;
	}

	public void setAgent(Agent agent)
	{
		this.agent = agent;
	}

	public Date getDate()
	{
		return date;
	}

	public void setDate(Date date)
	{
		this.date = date;
	}

	public Status getStatus()
	{
		return status;
	}

	public void setStatus(Status status)
	{
		this.status = status;
	}

	public Set<TransactionProduct> getTransactionProducts()
	{
		return transactionProducts;
	}

	public void setTransactionProducts(Set<TransactionProduct> transactionProducts)
	{
		this.transactionProducts = transactionProducts;
	}
	
	public Set<TransactionComment> getComments()
	{
		return comments;
	}

	public void setComments(Set<TransactionComment> comments)
	{
		this.comments = comments;
	}
}
