package com.pde.filters;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.pde.session.SessionBean;
import com.pde.utils.Utils;

public class LoginFilter implements Filter
{
	@Inject
	private SessionBean sessionBean;
	
	@Inject
	private Utils utils;
	
	@Override
	public void init(FilterConfig filterConfig) throws ServletException
	{

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException 
	{
		HttpServletRequest hsr = (HttpServletRequest)request;		
		String contextPath = hsr.getContextPath();
		
		if(!sessionBean.isLoggedIn()) 
		{
			((HttpServletResponse)response).sendRedirect(contextPath + "/login.xhtml");
		}
		
		if(!sessionBean.isAdmin())
		{
			if(utils.isModule("users", hsr.getRequestURI()))
			{
				((HttpServletResponse)response).sendRedirect(contextPath + "/exception/no-permission.xhtml");
			}
		}

		chain.doFilter(request, response);
	}

	@Override
	public void destroy()
	{

	}

}
