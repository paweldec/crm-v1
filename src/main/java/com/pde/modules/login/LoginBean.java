package com.pde.modules.login;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.pde.models.User;
import com.pde.models.User.Role;
import com.pde.services.UserService;
import com.pde.session.SessionBean;
import com.pde.utils.FacesLogger;
import com.pde.utils.Utils;

@Named("loginBean")
@RequestScoped
public class LoginBean implements Serializable
{
	private static final long serialVersionUID = -7091378290673101971L;

	@EJB(name = "UserService")
	private UserService userService;
	
	@Inject
	private SessionBean sessionBean;
	
	@Inject
	private Utils utils;
	
	private String login;
	private String password;
	
	public String login()
	{		
		User user = userService.login(login, utils.md5(password));
		
		if(user != null)
		{
			sessionBean.setUserId(user.getId());
			sessionBean.setUsername(login);
			sessionBean.setLoggedIn(true);
			sessionBean.setAdmin(user.getRole().equals(Role.ADMIN) ? true : false);
			
			return utils.url("announcements", "list", true);
		}
		
		FacesLogger.error("Nieprawidłowy login lub hasło");
	    return null;
	}
	
	public String logout()
	{
		sessionBean.setLoggedIn(false);
		sessionBean.setUsername("");
		
		return "/login.xhtml?faces-redirect=true";
	}
	
	public String getLogin()
	{
		return login;
	}
	
	public void setLogin(String login)
	{
		this.login = login;
	}
	
	public String getPassword()
	{
		return password;
	}
	
	public void setPassword(String password)
	{
		this.password = password;
	}
}
