package com.pde.modules.raports;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.ChartSeries;

import com.pde.models.Transaction;
import com.pde.models.TransactionProduct;
import com.pde.services.TransactionService;

@Named("reportSalesBean")
@ViewScoped
public class ReportSalesBean implements Serializable
{
	private static final long serialVersionUID = -4050180931934907416L;

	@EJB(name = "TransactionService")
	private TransactionService transactionService;
	
	private CartesianChartModel categoryModel; 
	private int maxValue;
	int weeks;
	private final Map<String, Object> reportDates;
  
	public ReportSalesBean()
	{
		weeks = 4;
		reportDates = new LinkedHashMap<String, Object>();
		reportDates.put("Ostatni miesiąc", 4);
		reportDates.put("Ostatnie 3 miesiące", 12);
		//reportDates.put("Ostatnie pół roku", 24);
	}
	
    public CartesianChartModel getCategoryModel() {  
        return categoryModel;  
    }  
    
    @PostConstruct
    public void createCategoryModel()
    {
    	SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
    	categoryModel = new CartesianChartModel(); 
    	ChartSeries sales = new ChartSeries();  
        sales.setLabel("Przychód ze sprzedaży (zł)");
        
        List<Date> dates = new ArrayList<Date>();
        int days = -7;       
        
        for(int i = 0; i <= weeks; i++)
        {
        	Calendar cal = GregorianCalendar.getInstance();
        	cal.add(Calendar.DAY_OF_YEAR, days * i);
        	dates.add(cal.getTime());
        }
        
        for(int i = weeks; i > 0; --i)
        {
        	List<Transaction> transactions = transactionService.getTransactionsBetweenDates(dates.get(i), dates.get(i-1));
        	double totalSalesWeek = 0;
        	
        	if(transactions != null && transactions.size() > 0)
        	{
	        	for(Transaction transaction : transactions)
	        	{        		
	        		for(TransactionProduct transactionProduct : transaction.getTransactionProducts())
	        		{
	        			totalSalesWeek += transactionProduct.getAmount() * transactionProduct.getProduct().getPrice();
	        		}	
	        	}
        	}
        	
        	if(totalSalesWeek > maxValue)
        	{
        		maxValue = (int)totalSalesWeek + 1;
        	}
        	
        	String startDate = sdf.format(dates.get(i));
        	String endDate = sdf.format(dates.get(i-1));

        	sales.set(startDate + " - " + endDate, totalSalesWeek);
        }
                
        categoryModel.addSeries(sales); 
    }

	public int getMaxValue()
	{
		return maxValue;
	}

	public int getWeeks()
	{
		return weeks;
	}

	public void setWeeks(int weeks)
	{
		this.weeks = weeks;
	}

	public Map<String, Object> getReportDates()
	{
		return reportDates;
	}
}
