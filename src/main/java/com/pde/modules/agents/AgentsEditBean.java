package com.pde.modules.agents;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import com.pde.models.Agent;
import com.pde.services.AgentService;
import com.pde.utils.FacesLogger;

@Named("agentsEditBean")
@ViewScoped
public class AgentsEditBean implements Serializable
{
	private static final long serialVersionUID = 3717582830666171116L;

	@EJB(name = "AgentService")
	private AgentService agentService;
	
	private long agentId;
	private Agent agent;
	
	public void init()
	{
		agent = agentService.getAgent(agentId);
		
		if(agent == null)
		{
			FacesLogger.error("Nieprawidłowe id przedstawiciela.");
		}
	}
	
	public void update()
	{
		if(!agentService.updateAgent(agent))
		{
			agent = null;
			FacesLogger.error("Przedstawiciel nie istnieje.");
			return;
		}

		FacesLogger.info("Zmiany zostały zapisane.");
	}
	
	public long getAgentId()
	{
		return agentId;
	}

	public void setAgentId(long agentId)
	{
		this.agentId = agentId;
	}

	public Agent getAgent()
	{
		return agent;
	}

	public void setAgent(Agent agent)
	{
		this.agent = agent;
	}
}
