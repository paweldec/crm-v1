package com.pde.modules.agents;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;

import com.pde.models.Agent;
import com.pde.models.Contractor;
import com.pde.services.AgentService;
import com.pde.services.ContractorService;
import com.pde.utils.FacesLogger;
import com.pde.utils.UrlParam;
import com.pde.utils.Utils;

@Named("agentsListBean")
@ViewScoped
public class AgentsListBean implements Serializable
{
	private static final long serialVersionUID = 7788116383116407077L;

	@EJB(name = "AgentService")
	private AgentService agentService;
	
	@EJB(name = "ContractorService")
	private ContractorService contractorService;
	
	@Inject
	private Utils utils;
	
	private Agent selectedAgent;
	private Contractor contractorForAgent;
	private List<Agent> filteredAgents;
	
	public List<Agent> getAllAgents()
	{
		return agentService.getAllAgents();
	}
	
	public Agent getSelectedAgent()
	{
		return selectedAgent;
	}

	public void setSelectedAgent(Agent selectedAgent)
	{
		this.selectedAgent = selectedAgent;
	}
	
	public String loadAgentToEdit()
	{
		if(selectedAgent == null)
		{
			FacesLogger.warn("Aby edytować dane przedstawiciela, musisz go najpierw wybrać z listy.");
			return null;
		}
		
		return utils.url("agents", "edit", true, new UrlParam("agentId", String.valueOf(selectedAgent.getId())));
	}
	
	public void removeAgent()
	{
		if(selectedAgent == null)
		{
			FacesLogger.warn("Aby usunąć przedstawiciela, musisz go najpierw wybrać z listy.");
			return;
		}
		
		if(!agentService.removeAgent(selectedAgent.getId()))
		{
			FacesLogger.warn("Przedstawiciel został już usunięty.");
			return;
		}
		
		FacesLogger.info("Przedstawiciel został usunięty.");
	}
	
	public void assignToContractor()
	{
		if(contractorForAgent == null)
		{
			FacesLogger.warn("Wybierz kontrahenta.");
			return;
		}
		
		if(contractorForAgent.getAgents().contains(selectedAgent))
		{
			FacesLogger.warn("Wybrany przedstawiciel jest już przypisany do tego kontrahenta.");
			return;
		}
		
		contractorForAgent.getAgents().add(selectedAgent);
		selectedAgent.getContractors().add(contractorForAgent);
		
		agentService.updateAgent(selectedAgent);
		
		if(!contractorService.updateContractor(contractorForAgent))
		{
			contractorForAgent = null;
			FacesLogger.error("Nie można przypisać przedstawiciela.");
			return;
		}
		
		FacesLogger.info("Przedstawiciel został przypisany do kontrahenta.");
		contractorForAgent = null;
		
		RequestContext context = RequestContext.getCurrentInstance();
	    context.execute("assignToContractorOverlayPanel.hide()");
	}
	
	public List<String> autoCompleteContractor(String query)
	{
		return contractorService.autoComplete(query);
	}

	public Contractor getContractorForAgent()
	{
		return contractorForAgent;
	}

	public void setContractorForAgent(Contractor contractorForAgent)
	{
		this.contractorForAgent = contractorForAgent;
	}
	
	public String goToNewAgentView()
	{
		return utils.url("agents", "new", true);
	}
	
	public String goToMailAgentView()
	{
		return utils.url("agents", "mail", true, new UrlParam("agentId", String.valueOf(selectedAgent.getId())));
	}

	public List<Agent> getFilteredAgents()
	{
		return filteredAgents;
	}

	public void setFilteredAgents(List<Agent> filteredAgents)
	{
		this.filteredAgents = filteredAgents;
	}
}
