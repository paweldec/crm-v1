package com.pde.modules.agents;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import com.pde.models.Agent;
import com.pde.services.AgentService;
import com.pde.utils.FacesLogger;

@Named("agentsNewBean")
@RequestScoped
public class AgentsNewBean implements Serializable
{
	private static final long serialVersionUID = -7902344212537348311L;
	
	@EJB(name = "AgentService")
	private AgentService agentService;
	
	private Agent agent = new Agent();

	public Agent getAgent()
	{
		return agent;
	}

	public void setAgent(Agent agent)
	{
		this.agent = agent;
	}
	
	public void createAgent()
	{
		if(!agentService.createAgent(agent))
		{
			FacesLogger.error("Nie można dodać przedstawiciela!");
		}
		else
		{
			agent = new Agent();
			FacesLogger.info("Przedstawiciel został dodany.");
		}
	}
}
