package com.pde.modules.agents;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import com.pde.models.Agent;
import com.pde.services.AgentService;
import com.pde.utils.FacesLogger;
import com.pde.utils.Mail;

@Named("agentsMailBean")
@ViewScoped
public class AgentsMailBean implements Serializable
{
	private static final long serialVersionUID = 185918993663546440L;

	@EJB(name = "AgentService")
	private AgentService agentService;
	
	@EJB(name = "Mail")
	private Mail mail;
	
	private long agentId;
	private Agent agent;
	
	private String mailContent;
	
	public void init()
	{
		agent = agentService.getAgent(agentId);
		
		if(agent == null)
		{
			FacesLogger.error("Nieprawidłowe id przedstawiciela.");
		}
	}
	
	public void sendMail()
	{
		if(!mail.send(agent.getEmail(), "CRM", mailContent))
		{
			FacesLogger.error("Nie można wysłać wiadomości.");
			return;
		}
		
		mailContent = "";
		
		FacesLogger.info("Wiadomość została wysłana.");
	}

	public long getAgentId()
	{
		return agentId;
	}

	public void setAgentId(long agentId)
	{
		this.agentId = agentId;
	}

	public Agent getAgent()
	{
		return agent;
	}

	public void setAgent(Agent agent)
	{
		this.agent = agent;
	}

	public String getMailContent()
	{
		return mailContent;
	}

	public void setMailContent(String mailContent)
	{
		this.mailContent = mailContent;
	}
}
