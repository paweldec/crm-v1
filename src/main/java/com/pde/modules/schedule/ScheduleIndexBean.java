package com.pde.modules.schedule;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.event.ActionEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.event.ScheduleEntryMoveEvent;
import org.primefaces.event.ScheduleEntryResizeEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultScheduleEvent;
import org.primefaces.model.DefaultScheduleModel;
import org.primefaces.model.ScheduleModel;

import com.pde.models.ScheduleEvent;
import com.pde.models.User;
import com.pde.services.ScheduleService;
import com.pde.services.UserService;
import com.pde.session.SessionBean;
import com.pde.utils.FacesLogger;

@Named("scheduleIndexBean")
@ViewScoped
public class ScheduleIndexBean implements Serializable
{
	private static final long serialVersionUID = -2463513504116497757L;

	@EJB(name="UserService")
	private UserService userService;
	
	@EJB(name="ScheduleService")
	private ScheduleService scheduleService;
	
	@Inject
	private SessionBean sessionBean;
	
	private ScheduleModel eventModel;
	private User user;
	
	private DefaultScheduleEvent event;
	
	@PostConstruct
	private void initSchedule()
	{
		event = new DefaultScheduleEvent(); 
		eventModel = new DefaultScheduleModel(); 		
		user = userService.getUser(sessionBean.getUserId());

		if(user == null) return;

		for(ScheduleEvent scheduleEvent : scheduleService.getUserScheduleEvents(user))
		{
			DefaultScheduleEvent newEvent = new DefaultScheduleEvent(
					scheduleEvent.getName(), scheduleEvent.getStartDate(), scheduleEvent.getEndDate());
			newEvent.setId(scheduleEvent.getEventId());
			eventModel.addEvent(newEvent);
			
			scheduleEvent.setEventId(newEvent.getId());
			scheduleService.update(scheduleEvent);
		}
	}

	public ScheduleModel getEventModel()
	{
		return eventModel;
	}

	public void setEventModel(ScheduleModel eventModel)
	{
		this.eventModel = eventModel;
	}

	public DefaultScheduleEvent getEvent()
	{
		return event;
	}

	public void setEvent(DefaultScheduleEvent event)
	{
		this.event = event;
	}
	
	public void addEvent(ActionEvent actionEvent) 
	{  
        if(event.getId() == null)  
        {
        	if(event.getTitle().length() < 1 || event.getStartDate() == null || event.getEndDate() == null)
        	{
        		return;
        	}
        	
            eventModel.addEvent(event);
    		
            ScheduleEvent scheduleEvent = new ScheduleEvent();
            scheduleEvent.setName(event.getTitle());
            scheduleEvent.setStartDate(event.getStartDate());
            scheduleEvent.setEndDate(event.getEndDate());
            scheduleEvent.setEventId(event.getId());
            scheduleEvent.setUser(this.user);

            if(!scheduleService.create(scheduleEvent))
            {
            	FacesLogger.error("Nie można zapisać zmian. To zdarzenie nie zostanie zapamiętane.");
            }
        }
        else
        {
            eventModel.updateEvent(event); 
            
            ScheduleEvent scheduleEvent = scheduleService.getScheduleEvent(event.getId());
            scheduleEvent.setName(event.getTitle());
            scheduleEvent.setStartDate(event.getStartDate());
            scheduleEvent.setEndDate(event.getEndDate());
            
            if(!scheduleService.update(scheduleEvent))
            {
            	FacesLogger.error("Nie można zapisać zmian.");
            }
        }
        
        event = new DefaultScheduleEvent();  
    } 
	
	private void saveScheduleEvent(DefaultScheduleEvent defaultScheduleEvent)
	{
		ScheduleEvent scheduleEvent = scheduleService.getScheduleEvent(defaultScheduleEvent.getId());
		
		if(scheduleEvent == null)
		{
			FacesLogger.error("Wystąpiły problemy. Zmiany nie zostaną zapisane.");
			return;
		}
		
		scheduleEvent.setName(defaultScheduleEvent.getTitle());
		scheduleEvent.setStartDate(defaultScheduleEvent.getStartDate());
		scheduleEvent.setEndDate(defaultScheduleEvent.getEndDate());
		scheduleEvent.setEventId(defaultScheduleEvent.getId());
		scheduleEvent.setUser(this.user);
		
		if(!scheduleService.update(scheduleEvent))
		{
			FacesLogger.error("Nie można zapisać zmian.");
		}
	}
	
	public void onEventMove(ScheduleEntryMoveEvent event) 
	{  
		saveScheduleEvent((DefaultScheduleEvent)event.getScheduleEvent());
    }  
      
    public void onEventResize(ScheduleEntryResizeEvent event) 
    {  
    	saveScheduleEvent((DefaultScheduleEvent)event.getScheduleEvent()); 
    }  
	
	public void onDateSelect(SelectEvent selectEvent)
	{
		event = new DefaultScheduleEvent("", (Date)selectEvent.getObject(), (Date)selectEvent.getObject());
	}
	
	public void onEventSelect(SelectEvent selectEvent)
	{
		event = (DefaultScheduleEvent)selectEvent.getObject(); 
	}
	
	public void removeEvent()
	{
		ScheduleEvent scheduleEvent = scheduleService.getScheduleEvent(event.getId());
		
		if(scheduleEvent == null)
		{
			FacesLogger.error("Nie można usunąć zdarzenia.");
			return;
		}
		
		eventModel.deleteEvent(event);
		
		if(!scheduleService.remove(scheduleEvent.getId()))
		{
			FacesLogger.error("Nie można zapisać zmian.");
			return;
		}
	}
}
