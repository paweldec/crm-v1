package com.pde.modules.announcements;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.pde.models.Announcement;
import com.pde.models.AnnouncementAnswer;
import com.pde.models.User;
import com.pde.services.AnnouncementService;
import com.pde.services.UserService;
import com.pde.session.SessionBean;
import com.pde.utils.FacesLogger;

@Named("announcementsListBean")
@ViewScoped
public class AnnouncementsListBean implements Serializable
{
	private static final long serialVersionUID = -4154979087781105531L;
	
	@EJB(name="announcementService")
	private AnnouncementService announcementService;
	
	@EJB(name="userService")
	private UserService userService;
	
	@Inject
	private SessionBean sessionBean;
	
	private String answerContent;
	private String announcementContent;
	private Date announcementToDate;
	
	public List<Announcement> getAnnouncementList()
	{
		return announcementService.getActualAndSorted();
	}
	
	public List<AnnouncementAnswer> getAnnouncementAnswers(Announcement announcement)
	{
		return announcement.getAnnouncementAnswers();
	}

	public String getAnswerContent()
	{
		return answerContent;
	}

	public void setAnswerContent(String answerContent)
	{
		this.answerContent = answerContent;
	}
	
	public void addNewAnswer(Announcement announcement)
	{
		User user = userService.getUser(sessionBean.getUserId());
		
		if(user == null)
		{
			FacesLogger.error("Brak danych użytkownika.");
			return;
		}
		
		AnnouncementAnswer answer = new AnnouncementAnswer();
		answer.setAnnouncement(announcement);
		answer.setContent(answerContent);
		answer.setDate(new Date());
		answer.setUser(user);	
		announcement.getAnnouncementAnswers().add(answer);
		
		if(!announcementService.update(announcement))
		{
			FacesLogger.error("Nie można dodać odpowiedzi.");
			return;
		}
		
		this.answerContent = "";
		
		FacesLogger.info("Dodano nową odpowiedź");
	}
	
	public void addNewAnnouncement()
	{
		if(announcementContent == null || announcementContent.length() <= 0)
		{
			FacesLogger.warn("Nie wpisano treści ogłoszenia");
			return;
		}
		
		if(announcementToDate == null)
		{
			FacesLogger.warn("Nie podano daty zakończenia.");
			return;
		}
		
		if(announcementToDate.compareTo(Calendar.getInstance().getTime()) < 0)
		{
			FacesLogger.warn("Nieprawidłowa data zakończenia.");
			return;
		}

		User user = userService.getUser(sessionBean.getUserId());
		
		if(user == null)
		{
			FacesLogger.error("Brak danych użytkownika.");
			return;
		}
		
		Announcement announcement = new Announcement();
		announcement.setContent(announcementContent);
		announcement.setDate(new Date());
		announcement.setToDate(announcementToDate);
		announcement.setUser(user);
		
		if(!announcementService.create(announcement))
		{
			FacesLogger.error("Nie można dodać ogłoszenia.");
			return;
		}
		
		this.announcementContent = "";
		
		FacesLogger.info("Dodano nowe ogłoszenie.");
	}
	
	public void onRowToggle()
	{
		this.answerContent = "";
	}

	public String getAnnouncementContent()
	{
		return announcementContent;
	}

	public void setAnnouncementContent(String announcementContent)
	{
		this.announcementContent = announcementContent;
	}

	public Date getAnnouncementToDate()
	{
		return announcementToDate;
	}

	public void setAnnouncementToDate(Date announcementToDate)
	{
		this.announcementToDate = announcementToDate;
	}
}
