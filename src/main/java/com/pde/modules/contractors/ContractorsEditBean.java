package com.pde.modules.contractors;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import com.pde.models.Activity;
import com.pde.models.Agent;
import com.pde.models.Contractor;
import com.pde.models.Province;
import com.pde.models.Trade;
import com.pde.models.Contractor.ContractorType;
import com.pde.services.ActivityService;
import com.pde.services.AgentService;
import com.pde.services.CityService;
import com.pde.services.ContractorService;
import com.pde.services.CountryService;
import com.pde.services.ProvinceService;
import com.pde.services.TradeService;
import com.pde.utils.FacesLogger;

@Named("contractorsEditBean")
@ViewScoped
public class ContractorsEditBean implements Serializable
{
	private static final long serialVersionUID = 6924127266200535113L;

	@EJB(name = "ContractorService")
	private ContractorService contractorService;
	
	@EJB(name = "CityService")
	private CityService cityService;
	
	@EJB(name = "CountryService")
	private CountryService countryService;
	
	@EJB(name = "ProvinceService")
	private ProvinceService provinceService;
	
	@EJB(name = "TradeService")
	private TradeService tradeService;
	
	@EJB(name = "ActivityService")
	private ActivityService activityService;
	
	@EJB(name = "AgentService")
	private AgentService agentService;
	
	private long contractorId;
	private Contractor contractor;
	private String contractorType;
	
	public void init()
	{
		contractor = contractorService.getContractor(contractorId);
		
		if(contractor == null)
		{
			FacesLogger.error("Nieprawidłowe id kontrahenta.");
		}
		else
		{
			switch(contractor.getType())
			{
				case PERSON:
				{
					this.contractorType = "person";
				}
				case COMPANY:
				{
					this.contractorType = "company";
				}
			}
		}
	}
	
	public void update()
	{
		switch(contractorType)
		{
			case "person":
			{
				contractor.setType(ContractorType.PERSON);
				break;
			}
			default:
			{
				contractor.setType(ContractorType.COMPANY);
			}
		}
		
		if(!contractorService.updateContractor(contractor))
		{
			contractor = null;
			FacesLogger.error("Kontrahent nie istnieje.");
			return;
		}

		FacesLogger.info("Zmiany zostały zapisane.");
	}
	
	public long getContractorId()
	{
		return contractorId;
	}

	public void setContractorId(long contractorId)
	{
		this.contractorId = contractorId;
	}

	public Contractor getContractor()
	{
		return contractor;
	}

	public void setContractor(Contractor contractor)
	{
		this.contractor = contractor;
	}
	
	public String getContractorType()
	{
		return contractorType;
	}

	public void setContractorType(String contractorType)
	{
		this.contractorType = contractorType;
	}
	
	public List<Trade> getAllTrades()
	{
		return tradeService.getAllTrades();
	}
	
	public List<Activity> getAllActivities()
	{
		return activityService.getAllActivities();
	}

	public List<String> autoCompleteCity(String query)
	{
		return cityService.autoComplete(query);
	}
	
	public List<String> autoCompleteCountry(String query)
	{
		return countryService.autoComplete(query);
	}
	
	public List<Province> getAllProvinces()
	{
		return provinceService.getAllProvinces();
	}
	
	public List<Agent> getContractorAgentsList() 
	{
		return this.contractor.getAgents();
	}
	
	public List<Agent> getAgentsList()
	{
		return agentService.getAllAgents();
	}
	
	public void removeContractorAgent(long id)
	{
		Agent agent = agentService.getAgent(id);
		
		if(agent == null)
		{
			FacesLogger.error("Nie można usunąć przedstawiciela.");
			return;
		}
		
		contractor.getAgents().remove(agent);
	
		if(!contractorService.updateContractor(contractor))
		{
			FacesLogger.error("Nie można usunąć przedstawiciela.");
			return;
		}
		
		FacesLogger.info("Przedstawiciel został usunięty.");		
	}
	
	public void onContractorTypeSelect()
	{
		
	}
}
