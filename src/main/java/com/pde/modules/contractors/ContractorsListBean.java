package com.pde.modules.contractors;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.pde.models.Contractor;
import com.pde.services.ContractorService;
import com.pde.utils.FacesLogger;
import com.pde.utils.UrlParam;
import com.pde.utils.Utils;

@Named("contractorsListBean")
@ViewScoped
public class ContractorsListBean implements Serializable
{
	private static final long serialVersionUID = -8225881135590450752L;

	@EJB(name = "ContractorService")
	private ContractorService contractorService;
	
	@Inject
	private Utils utils;
	
	private Contractor selectedContractor = null;
	private List<Contractor> filteredContractors;
	
	public List<Contractor> getAllContractors()
	{
		return contractorService.getAllContractors();
	}

	public Contractor getSelectedContractor()
	{
		return selectedContractor;
	}

	public void setSelectedContractor(Contractor selectedContractor)
	{
		this.selectedContractor = selectedContractor;
	}
	
	public String loadContractorToEdit()
	{
		if(selectedContractor == null)
		{
			FacesLogger.warn("Aby edytować dane kontrahenta, musisz go najpierw wybrać z listy.");
			return null;
		}
		
		return utils.url("contractors", "edit", true, new UrlParam("contractorId", String.valueOf(selectedContractor.getId())));
	}
	
	public void removeContractor()
	{
		if(selectedContractor == null)
		{
			FacesLogger.warn("Aby usunąć kontrahenta, musisz go najpierw wybrać z listy.");
			return;
		}
		
		if(!contractorService.removeContractor(selectedContractor.getId()))
		{
			FacesLogger.error("Nie można usunąć kontrahenta.");
			return;
		}
		
		FacesLogger.info("Kontrahent został usunięty.");
	}
	
	public String goToNewContractorView()
	{
		return utils.url("contractors", "new", true);
	}

	public List<Contractor> getFilteredContractors()
	{
		return filteredContractors;
	}

	public void setFilteredContractors(List<Contractor> filteredContractors)
	{
		this.filteredContractors = filteredContractors;
	}
}
