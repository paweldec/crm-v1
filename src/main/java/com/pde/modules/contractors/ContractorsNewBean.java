package com.pde.modules.contractors;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import com.pde.models.Activity;
import com.pde.models.City;
import com.pde.models.Contractor;
import com.pde.models.Contractor.ContractorType;
import com.pde.models.Province;
import com.pde.models.Trade;
import com.pde.services.ActivityService;
import com.pde.services.CityService;
import com.pde.services.ContractorService;
import com.pde.services.CountryService;
import com.pde.services.ProvinceService;
import com.pde.services.TradeService;
import com.pde.utils.FacesLogger;

@Named("contractorsNewBean")
@RequestScoped
public class ContractorsNewBean implements Serializable
{
	private static final long serialVersionUID = -7099467692430434943L;

	@EJB(name = "ContractorService")
	private ContractorService contractorService;
	
	@EJB(name = "CityService")
	private CityService cityService;
	
	@EJB(name = "CountryService")
	private CountryService countryService;
	
	@EJB(name = "ProvinceService")
	private ProvinceService provinceService;
	
	@EJB(name = "TradeService")
	private TradeService tradeService;
	
	@EJB(name = "ActivityService")
	private ActivityService activityService;
	
	private Contractor contractor = new Contractor();
	private String contractorType = "company";
	
	public Contractor getContractor()
	{
		return contractor;
	}

	public void setContractor(Contractor contractor)
	{
		this.contractor = contractor;
	}
	
	public void createContractor() 
	{	
		System.out.println("create contractor: " + contractor.getPESEL());
		
		switch(contractorType)
		{
			case "person":
			{
				contractor.setType(ContractorType.PERSON);
				break;
			}
			default:
			{
				contractor.setType(ContractorType.COMPANY);
			}
		}
		
		if(!contractorService.createContractor(contractor))
		{
			FacesLogger.error("Nie można dodać kontrahenta!");
		}
		else
		{
			contractor = new Contractor();
			FacesLogger.info("Kontrahent został dodany.");
		}
	}
	
	public List<String> autoCompleteCity(String query)
	{
		return cityService.autoComplete(query);
	}
	
	public List<String> autoCompleteCountry(String query)
	{
		return countryService.autoComplete(query);
	}
	
	public List<Province> getAllProvinces()
	{
		return provinceService.getAllProvinces();
	}
	
	public ContractorType[] getContractorTypes()
	{
		return ContractorType.values();
	}
	
	public void onContractorTypeSelect()
	{
		/*
		if(contractorType.contains("person")) 
		{
			contractor.setType(Contractor.ContractorType.PERSON);
		}
		else
		{
			contractor.setType(Contractor.ContractorType.COMPANY);
		}
		*/
	}
	
	public String getContractorType()
	{
		return contractorType;
	}

	public void setContractorType(String contractorType)
	{
		this.contractorType = contractorType;
	}
	
	public List<Trade> getAllTrades()
	{
		return tradeService.getAllTrades();
	}
	
	public List<Activity> getAllActivities()
	{
		return activityService.getAllActivities();
	}
}
