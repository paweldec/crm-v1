package com.pde.modules.transactions;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.pde.models.Transaction;
import com.pde.services.TransactionService;
import com.pde.utils.UrlParam;
import com.pde.utils.Utils;

@Named("transactionsListBean")
@ViewScoped
public class TransactionsListBean implements Serializable
{
	private static final long serialVersionUID = -3072691773257785025L;

	@EJB(name = "TransactionService")
	private TransactionService transactionService;
	
	@Inject
	private Utils utils;
	
	private Transaction selectedTransaction;
	private List<Transaction> filteredTransactions;
	private String listType = "WAITING";
	
	private static Map<String, Object> listTypes;
	static{
		listTypes = new LinkedHashMap<String,Object>();
		listTypes.put("Oczekujące", "WAITING");
		listTypes.put("Zakończone", "COMPLETE");
	}
	
	public String getTransactionListTitle()
	{
		if(listType.equals("COMPLETE"))
		{
			return "Lista zakończonych transakcji";
		}
		
		return "Lista oczekujących transakcji";
	}
	
	public void changeTransactionsList()
	{
		
	}
	
	public Map<String, Object> getListTypes() 
	{
		return listTypes;
	}
	
	public List<Transaction> getTransactionsList()
	{
		if(listType.equals("COMPLETE"))
		{
			return transactionService.getTransactionsByStatus(Transaction.Status.COMPLETE);
		}
		
		return transactionService.getTransactionsByStatus(Transaction.Status.WAITING);
	}

	public Transaction getSelectedTransaction()
	{
		return selectedTransaction;
	}

	public void setSelectedTransaction(Transaction selectedTransaction)
	{
		this.selectedTransaction = selectedTransaction;
	}
	
	public String showDetails()
	{
		return utils.url("transactions", "details", true, new UrlParam("transactionId", String.valueOf(selectedTransaction.getId())));
	}

	public List<Transaction> getFilteredTransactions()
	{
		return filteredTransactions;
	}

	public void setFilteredTransactions(List<Transaction> filteredTransactions)
	{
		this.filteredTransactions = filteredTransactions;
	}

	public String getListType()
	{
		return listType;
	}

	public void setListType(String listType)
	{
		this.listType = listType;
	}
}
