package com.pde.modules.transactions;

import java.io.Serializable;
import java.util.Date;

import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.context.RequestContext;

import com.pde.models.Transaction;
import com.pde.models.TransactionComment;
import com.pde.models.TransactionProduct;
import com.pde.services.TransactionService;
import com.pde.utils.FacesLogger;

@Named("transactionsDetailsBean")
@ViewScoped
public class TransactionsDetailsBean implements Serializable
{
	private static final long serialVersionUID = 2988477549862631257L;

	@EJB(name = "TransactionService")
	private TransactionService transactionService;
	
	private Transaction transaction;
	private long transactionId;
	private double totalCost;
	private String commentContent;
	
	public void init()
	{
		transaction = transactionService.getTransaction(transactionId);

		if(transaction == null)
		{
			FacesLogger.error("Nieprawidłowe id transakcji.");
			return;
		}
		
		totalCost = 0;
		
		for(TransactionProduct transactionProduct : transaction.getTransactionProducts())
		{
			totalCost += (transactionProduct.getAmount() * transactionProduct.getProduct().getPrice());
		}
	}

	public Transaction getTransaction()
	{
		return transaction;
	}

	public void setTransaction(Transaction transaction)
	{
		this.transaction = transaction;
	}

	public long getTransactionId()
	{
		return transactionId;
	}

	public void setTransactionId(long transactionId)
	{
		this.transactionId = transactionId;
	}
	
	public void finalizeTransaction()
	{
		transaction.setStatus(Transaction.Status.COMPLETE);
		if(!transactionService.updateTransaction(transaction))
		{
			FacesLogger.error("Nie można zapisać zmian.");
		}
		
		FacesLogger.info("Transakcja została sfinalizowana");
	}

	public double getTotalCost()
	{
		return totalCost;
	}
	
	public String getCommentContent()
	{
		return commentContent;
	}

	public void setCommentContent(String commentContent)
	{
		this.commentContent = commentContent;
	}

	public void addNewComment()
	{
		TransactionComment comment = new TransactionComment();
		comment.setContent(commentContent);
		comment.setDate(new Date());
		comment.setTransaction(transaction);
		transaction.getComments().add(comment);
		
		if(!transactionService.updateTransaction(transaction))
		{
			FacesLogger.error("Nie można dodać komentarza.");
			return;
		}
		
		FacesLogger.info("Komentarz został dodany.");
		commentContent = "";
		
		RequestContext context = RequestContext.getCurrentInstance();
	    context.execute("newCommentOverlayPanel.hide()");
	}
}
