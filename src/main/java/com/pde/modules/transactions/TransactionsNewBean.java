package com.pde.modules.transactions;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.event.FlowEvent;

import com.pde.models.Agent;
import com.pde.models.Contractor;
import com.pde.models.Product;
import com.pde.models.Transaction;
import com.pde.models.Transaction.Status;
import com.pde.models.TransactionProduct;
import com.pde.services.ContractorService;
import com.pde.services.AgentService;
import com.pde.services.ProductService;
import com.pde.services.TransactionService;
import com.pde.utils.FacesLogger;

@Named("transactionsNewBean")
@ViewScoped
public class TransactionsNewBean implements Serializable
{
	private static final long serialVersionUID = -294350597313864581L;

	@EJB(name = "ContractorService")
	private ContractorService contractorService;

	@EJB(name = "ContractorService")
	private AgentService agentService;

	@EJB(name = "ProductService")
	private ProductService productService;
	
	@EJB(name = "TransactionService")
	private TransactionService transactionService;
	
	private Contractor contractor;
	private Agent agent;
	private List<Integer> transactionProductMaxAmount;
	private List<Product> transactionProducts;
	private List<Product> transactionReadyProducts;
	private double totalPrice = 0;

	public Contractor getContractor()
	{
		return contractor;
	}

	public void setContractor(Contractor contractor)
	{
		this.contractor = contractor;
	}

	public Agent getAgent()
	{
		return agent;
	}

	public void setAgent(Agent agent)
	{
		this.agent = agent;
	}

	public List<String> autoCompleteContractor(String query)
	{
		return contractorService.autoComplete(query);
	}

	public List<Agent> getContractorAgents()
	{
		if (contractor == null)
		{
			return null;
		}

		return contractor.getAgents();
	}
	
	public boolean checkAndCopyReadyProducts()
	{
		int totalProductAmount = 0;
		transactionReadyProducts = new ArrayList<Product>();
				
		for(Product product : transactionProducts)
		{
			if(product.getAmount() > 0)
			{
				totalProductAmount += product.getAmount();
				totalPrice += (product.getAmount() * product.getPrice());
				transactionReadyProducts.add(product);			
			}
		}
		
		if(totalProductAmount <= 0)
		{
			return false;
		}
		
		return true;
	}

	public String onFlowProcess(FlowEvent event)
	{		

		if (event.getOldStep().equals("contractor") && event.getNewStep().equals("agent") && contractor == null) 
		{
            FacesLogger.warn("Aby przejść dalej wybierz kontrahenta.");
            return event.getOldStep();
        }
		else if(event.getOldStep().equals("product") && event.getNewStep().equals("confirm"))
		{
			if(!checkAndCopyReadyProducts())
			{
				FacesLogger.warn("Nie wybrano żadnych produktów");
				return event.getOldStep();
			}
		}
		
		return event.getNewStep();
	}
	
	public List<Product> getAllTransactionProducts()
	{
		if(transactionProducts == null)
		{
			transactionProducts = productService.getAllProducts();
			transactionProductMaxAmount = new ArrayList<Integer>();
			
			for(Product transactionProduct : transactionProducts)
			{
				transactionProductMaxAmount.add(transactionProduct.getAmount());
				transactionProduct.setAmount(0);
			}
		}
		
		return transactionProducts;
	}
	
	public int getProductMaxAmount(int index)
	{
		return transactionProductMaxAmount.get(index);
	}

	public List<Product> getTransactionReadyProducts()
	{
		return transactionReadyProducts;
	}

	public void setTransactionReadyProducts(List<Product> transactionReadyProducts)
	{
		this.transactionReadyProducts = transactionReadyProducts;
	}
	
	public void confirmTransaction()
	{
		Transaction transaction = new Transaction();
		transaction.setContractor(contractor);
		transaction.setAgent(agent);
		transaction.setDate(new Date());
		
		Set<TransactionProduct> transactionProducts = new HashSet<TransactionProduct>();
		
		for(Product product : transactionReadyProducts)
		{
			// czy w miedzy czasie nie ubylo produktow?
			int currProductAmount = productService.getProductAmount(product.getId());
			if(currProductAmount < product.getAmount()) {
				FacesLogger.warn("Produkt '" + product.getName() + "' został wykupiony w miedzyczasie. Aktualnie dostępnych jest " +
						currProductAmount + " " + product.getUnit().getName());
				return;
			}
				
			TransactionProduct transactionProduct = new TransactionProduct();
			transactionProduct.setProduct(product);
			transactionProduct.setAmount(product.getAmount());
			transactionProduct.setTransaction(transaction);
			transactionProducts.add(transactionProduct);
		}
		
		transaction.setTransactionProducts(transactionProducts);
		transaction.setStatus(Status.WAITING);
		
		if(!transactionService.createTransaction(transaction))
		{
			FacesLogger.error("Nie można zapisać transakcji");
			return;
		}
	
		FacesLogger.info("Transakcja została dodana do listy oczekujących na finalizację.");
	}

	public double getTotalPrice()
	{
		return totalPrice;
	}

	public void setTotalPrice(double totalPrice)
	{
		this.totalPrice = totalPrice;
	}
}
