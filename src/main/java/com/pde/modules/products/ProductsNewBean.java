package com.pde.modules.products;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import com.pde.models.Product;
import com.pde.models.Unit;
import com.pde.services.ProductService;
import com.pde.services.UnitService;
import com.pde.utils.FacesLogger;

@Named("productsNewBean")
@RequestScoped
public class ProductsNewBean implements Serializable
{
	private static final long serialVersionUID = -2302090655109178169L;

	@EJB(name = "ProductService")
	private ProductService productService;
	
	@EJB(name = "UnitService")
	private UnitService unitService;

	private Product product = new Product();
	
	public void addProduct()
	{
		if(!productService.createProduct(product))
		{
			FacesLogger.error("Nie można dodać produktu!");
		}
		else
		{
			product = new Product();
			FacesLogger.info("Produkt został dodany.");
		}
	}

	public Product getProduct()
	{
		return product;
	}

	public void setProduct(Product product)
	{
		this.product = product;
	}
	
	public List<Unit> getAllUnits()
	{
		return unitService.getAllUnits();
	}
}
