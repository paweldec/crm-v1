package com.pde.modules.products;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.pde.models.Product;
import com.pde.models.Unit;
import com.pde.services.ProductService;
import com.pde.services.UnitService;
import com.pde.utils.FacesLogger;
import com.pde.utils.UrlParam;
import com.pde.utils.Utils;

@Named("productsListBean")
@ViewScoped
public class ProductsListBean implements Serializable
{
	private static final long serialVersionUID = -9148392197412524003L;

	@Inject
	private Utils utils;
	
	@EJB(name = "ProductService")
	private ProductService productService;
	
	@EJB(name = "UnitService")
	private UnitService unitService;
	
	private Product selectedProduct;
	private List<Product> filteredProducts;
	
	public List<Unit> getAllUnits()
	{
		return unitService.getAllUnits();
	}
	
	public List<Product> getAllProducts()
	{
		return productService.getAllProducts();
	}

	public Product getSelectedProduct()
	{
		return selectedProduct;
	}

	public void setSelectedProduct(Product selectedProduct)
	{
		this.selectedProduct = selectedProduct;
	}
	
	public void removeProduct()
	{
		if(selectedProduct == null)
		{
			FacesLogger.warn("Aby usunąć produkt, musisz go najpierw wybrać z listy.");
			return;
		}
		
		if(!productService.removeProduct(selectedProduct.getId()))
		{
			FacesLogger.warn("Produkt został już usunięty.");
			return;
		}
		
		FacesLogger.info("Produkt został usunięty.");
	}
	
	public String loadProductToEdit()
	{
		if(selectedProduct == null)
		{
			FacesLogger.warn("Aby edytować produkt, musisz go najpierw wybrać z listy.");
			return null;
		}
		
		return utils.url("products", "edit", true, new UrlParam("productId", String.valueOf(selectedProduct.getId())));
	}
	
	public String goToNewProductView()
	{
		return utils.url("products", "new", true);
	}

	public List<Product> getFilteredProducts()
	{
		return filteredProducts;
	}

	public void setFilteredProducts(List<Product> filteredProducts)
	{
		this.filteredProducts = filteredProducts;
	}
}
