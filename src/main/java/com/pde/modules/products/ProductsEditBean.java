package com.pde.modules.products;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import com.pde.models.Product;
import com.pde.models.Unit;
import com.pde.services.ProductService;
import com.pde.services.UnitService;
import com.pde.utils.FacesLogger;

@Named("productsEditBean")
@ViewScoped
public class ProductsEditBean implements Serializable
{
	private static final long serialVersionUID = -9148392197412524003L;

	@EJB(name = "ProductService")
	private ProductService productService;
	
	@EJB(name = "UnitService")
	private UnitService UnitService;
	
	private long productId;
	private Product product;
	
	public void init()
	{
		product = productService.getProduct(productId);
		
		if(product == null)
		{
			FacesLogger.error("Nieprawidłowe id produktu.");
		}
	}
	
	public void update()
	{
		if(!productService.updateProduct(product))
		{
			product = null;
			FacesLogger.error("Produkt nie istnieje.");
			return;
		}

		FacesLogger.info("Zmiany zostały zapisane.");
	}
	
	public List<Unit> getAllUnits()
	{
		return UnitService.getAllUnits();
	}

	public long getProductId()
	{
		return productId;
	}

	public void setProductId(long productId)
	{
		this.productId = productId;
	}

	public Product getProduct()
	{
		return product;
	}

	public void setProduct(Product product)
	{
		this.product = product;
	}
}
