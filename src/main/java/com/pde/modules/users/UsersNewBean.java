package com.pde.modules.users;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.pde.models.User;
import com.pde.services.UserService;
import com.pde.utils.FacesLogger;
import com.pde.utils.Utils;

@Named("usersNewBean")
@RequestScoped
public class UsersNewBean implements Serializable
{
	private static final long serialVersionUID = -7902344212537348311L;
	
	@EJB(name = "UserService")
	private UserService userService;
	
	@Inject
	private Utils utils;
	
	private User user = new User();

	public User getUser()
	{
		return user;
	}

	public void setUser(User user)
	{
		this.user = user;
	}
	
	public User.Role[] getUserRoleValues()
	{
		return User.Role.values();
	}
	
	public void createUser()
	{
		String pass = user.getPassword();
		user.setPassword(utils.md5(pass));
		
		if(!userService.createUser(user))
		{
			FacesLogger.error("Użytkownik o takim loginie już istnieje.");
		}
		else
		{
			user = new User();
			FacesLogger.info("Użytkownik został dodany.");
		}
		
		user.setPassword(pass);
	}
}
