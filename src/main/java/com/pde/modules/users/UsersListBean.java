package com.pde.modules.users;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.pde.models.User;
import com.pde.services.UserService;
import com.pde.utils.FacesLogger;
import com.pde.utils.UrlParam;
import com.pde.utils.Utils;

@Named("usersListBean")
@ViewScoped
public class UsersListBean implements Serializable 
{
	private static final long serialVersionUID = -5917348258858396912L;
	
	@EJB(name = "UserService")
	private UserService userService;
	
	@Inject
	private Utils utils;
	
	private User selectedUser = null;

	public List<User> getAllUsers()
	{
		return userService.getAllUsers();
	}
	
	public User getSelectedUser()
	{
		return selectedUser;
	}

	public void setSelectedUser(User selectedUser)
	{
		this.selectedUser = selectedUser;
	}
	
	public String loadUserToEdit()
	{
		if(selectedUser == null)
		{
			FacesLogger.warn("Aby edytować dane użytkownika, musisz go najpierw wybrać z listy.");
			return null;
		}
		
		return utils.url("users", "edit", true, new UrlParam("userId", String.valueOf(selectedUser.getId())));
	}
	
	public void removeUser()
	{
		if(selectedUser == null)
		{
			FacesLogger.warn("Aby usunąć użytkownika, musisz go najpierw wybrac z listy.");
			return;
		}
		
		if(!userService.removeUser(selectedUser.getId()))
		{
			FacesLogger.warn("Użytkownik został już usunięty.");
			return;
		}
		
		FacesLogger.info("Użytkownik został usunięty.");
	}
	
	public String goToNewUserView()
	{
		return utils.url("users", "new", true);
	}
}
