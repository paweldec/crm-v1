package com.pde.modules.users;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import com.pde.models.User;
import com.pde.services.UserService;
import com.pde.utils.FacesLogger;

@Named("usersEditBean")
@ViewScoped
public class UsersEditBean implements Serializable
{
	private static final long serialVersionUID = 9012694213745201175L;
	
	@EJB(name = "UserService")
	private UserService userService;
	
	private long userId;
	private User user;

	public void init()
	{
		user = userService.getUser(userId);
		
		if(user == null)
		{
			FacesLogger.error("Nieprawidłowe id użytkownika.");
		}
	}
	
	public void update()
	{
		if(!userService.updateUser(user))
		{
			user = null;
			FacesLogger.error("Użytkownik nie istnieje.");
			return;
		}

		FacesLogger.info("Zmiany zostały zapisane.");
	}
	
	public User.Role[] getUserRoleValues()
	{
		return User.Role.values();
	}

	public long getUserId()
	{
		return userId;
	}

	public void setUserId(long userId)
	{
		this.userId = userId;
	}
	
	public User getUser()
	{
		return user;
	}

	public void setUser(User user)
	{
		this.user = user;
	}
}
