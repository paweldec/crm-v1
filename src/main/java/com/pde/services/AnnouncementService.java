package com.pde.services;

import java.util.List;

import com.pde.models.Announcement;

public interface AnnouncementService
{
	public List<Announcement> getAllAnnouncements();
	
	public boolean update(Announcement announcement);
	
	public boolean create(Announcement announcement);
	
	public List<Announcement> getActualAndSorted();
}
