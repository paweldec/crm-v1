package com.pde.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.pde.models.Product;

@Stateless(name = "ProductService")
public class ProductServiceImpl implements ProductService
{
	@Inject
	private EntityManager em;
	
	@Override
	public boolean createProduct(Product product)
	{
		if(product == null)
		{
			return false;
		}
		
		List<Product> productList = em.createNamedQuery("Product.findByName", Product.class)
				.setParameter("name", product.getName())
				.getResultList();
		
		if(productList.size() > 1)
		{
			System.out.println("Duplicated products in database!");
		}
		
		if(productList.size() != 0)
		{
			return false;
		}
		
		try
		{
			em.persist(product);
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
			return false;
		}
		
		
		return true;
	}

	@Override
	public List<Product> getAllProducts()
	{
		return em.createNamedQuery("Product.findAll", Product.class)
				.getResultList();
	}

	@Override
	public boolean removeProduct(long productId)
	{
		Product product = em.find(Product.class, productId);
		
		if(product == null)
		{
			return false;
		}
		
		em.remove(product);
		return true;
	}

	@Override
	public Product getProduct(long productId)
	{
		return em.find(Product.class, productId);
	}

	@Override
	public boolean updateProduct(Product product)
	{
		try
		{
			em.merge(product);
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
			return false;
		}
		
		return true;
	}

	@Override
	public int getProductAmount(long productId)
	{
		Product product = em.find(Product.class, productId);
		
		if(product == null){
			return 0;
		}
		
		return product.getAmount();
	}
}
