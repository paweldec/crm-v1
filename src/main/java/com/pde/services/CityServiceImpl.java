package com.pde.services;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.pde.models.City;

@Stateless(name = "CityService")
public class CityServiceImpl implements CityService
{
	@Inject
	private EntityManager em;
	
	@Override
	public List<String> autoComplete(String query)
	{
		List<City> cities = em.createNamedQuery("City.findByNameLike", City.class)
				.setParameter("name", query.toUpperCase() + '%')
				.getResultList();
		
		List<String> citiesNames = new ArrayList<String>();
		
		for(City city : cities)
		{
			citiesNames.add(city.getName());
		}
		
		return citiesNames;
	}

	@Override
	public City getCityByName(String name)
	{
		List<City> cities = em.createNamedQuery("City.findByName", City.class)
				.setParameter("name", name)
				.getResultList();
		
		if(cities.size() > 1)
		{
			System.out.println("Duplicated cities in database");
		}
		
		if(cities.size() != 1) 
		{
			return null;
		}
		
		return cities.get(0);
	}
}
