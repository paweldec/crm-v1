package com.pde.services;

import java.util.List;

import com.pde.models.ScheduleEvent;
import com.pde.models.User;

public interface ScheduleService 
{
	public List<ScheduleEvent> getUserScheduleEvents(User user);
	
	public ScheduleEvent getScheduleEvent(String eventId);
	
	public boolean update(ScheduleEvent scheduleEvent);
	
	public boolean create(ScheduleEvent scheduleEvent);
	
	public boolean remove(long scheduleEventId);
}
