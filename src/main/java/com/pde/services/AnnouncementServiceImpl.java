package com.pde.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.pde.models.Announcement;

@Stateless(name = "AnnouncementService")
public class AnnouncementServiceImpl implements AnnouncementService
{
	@Inject
	private EntityManager em;

	@Override
	public List<Announcement> getAllAnnouncements()
	{
		return em.createNamedQuery("Announcement.findAll", Announcement.class)
		.getResultList();
	}

	@Override
	public boolean update(Announcement announcement)
	{
		try
		{
			em.merge(announcement);
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
			return false;
		}
		
		return true;
	}

	@Override
	public boolean create(Announcement announcement)
	{
		if(announcement == null)
		{
			return false;
		}
		
		if(announcement.getContent().length() <= 0)
		{
			return false;
		}
		
		try
		{
			em.persist(announcement);
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
			return false;
		}
			
		return true;
	}

	@Override
	public List<Announcement> getActualAndSorted()
	{
		return em.createNamedQuery("Announcement.findAcutalAndSortedByDate", Announcement.class)
		.getResultList();
	}
}
