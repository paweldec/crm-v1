package com.pde.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.pde.models.Agent;
import com.pde.models.Visibility;

@Stateless(name = "AgentService")
public class AgentServiceImpl implements AgentService
{	
	@Inject
	private EntityManager em;

	@Override
	public List<Agent> getAllAgents()
	{
		return em.createNamedQuery("Agent.findAll", Agent.class).getResultList();
	}

	@Override
	public boolean createAgent(Agent agent)
	{
		if(agent == null)
		{
			return false;
		}
		
		try
		{
			em.persist(agent);
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
			return false;
		}
		
		return true;
	}

	@Override
	public boolean removeAgent(Long agentId)
	{
		Agent agent = em.find(Agent.class, agentId);
		
		if(agent == null)
		{
			return false;
		}
		
		agent.setVisibility(Visibility.HIDDEN);
		
		em.merge(agent);
		return true;
	}

	@Override
	public Agent getAgent(Long agentId)
	{
		Agent agent = em.find(Agent.class, agentId);
		return agent;
	}
	
	@Override
	public boolean updateAgent(Agent agent)
	{
		try
		{
			em.merge(agent);
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
			return false;
		}
		
		return true;
	}
}
