package com.pde.services;

import java.util.List;

import com.pde.models.Country;

public interface CountryService
{
	public List<String> autoComplete(String query);
	
	public Country getCountryByName(String name);
}
