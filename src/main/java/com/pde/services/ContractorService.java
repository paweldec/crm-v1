package com.pde.services;

import java.util.List;

import com.pde.models.Contractor;

public interface ContractorService
{
	public List<Contractor> getAllContractors();
	
	public boolean createContractor(Contractor contractor);
	
	public boolean removeContractor(Long contractorId);
	
	public Contractor getContractor(Long contractorId);
	
	public boolean updateContractor(Contractor contractor);
	
	public List<String> autoComplete(String query);
	
	public Contractor getContractorByName(String name);
}
