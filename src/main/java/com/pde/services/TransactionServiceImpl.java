package com.pde.services;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TemporalType;
import javax.transaction.RollbackException;

import com.pde.models.Product;
import com.pde.models.Transaction;
import com.pde.models.TransactionProduct;

@Stateless(name = "TransactionService")
public class TransactionServiceImpl implements TransactionService
{
	@Inject
	private EntityManager em;
	
	@Override
	public boolean createTransaction(Transaction transaction)
	{
		try
		{
			em.persist(transaction);
			
			for(TransactionProduct transactionProduct : transaction.getTransactionProducts())
			{
				Product product = em.find(Product.class, transactionProduct.getProduct().getId());
				
				if(product == null) {
					throw new RollbackException();
				}
				
				product.setAmount(product.getAmount() - transactionProduct.getAmount());
				em.merge(product);
			}
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
			return false;
		}
		
		return true;
	}

	@Override
	public List<Transaction> getTransactionsBetweenDates(Date start, Date end)
	{
		return em.createNamedQuery("Transaction.findAllBetweenDate", Transaction.class)
				.setParameter("start", start, TemporalType.TIMESTAMP)
				.setParameter("end", end, TemporalType.TIMESTAMP)
				.getResultList();
	}

	@Override
	public List<Transaction> getTransactionsByStatus(Transaction.Status status)
	{
		return em.createNamedQuery("Transaction.findTransactionsByStatus", Transaction.class)
				.setParameter("status", status)
				.getResultList();
	}
	
	@Override
	public Transaction getTransaction(long transactionId)
	{
		return em.find(Transaction.class, transactionId);
	}

	@Override
	public boolean updateTransaction(Transaction transaction)
	{
		try
		{
			em.merge(transaction);
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
			return false;
		}
		
		return true;
	}
}
