package com.pde.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.pde.models.Province;

@Stateless(name = "ProvinceService")
public class ProvinceServiceImpl implements ProvinceService
{
	@Inject
	private EntityManager em;
	
	@Override
	public Province getProvinceByName(String name)
	{
		List<Province> provinces = em.createNamedQuery("Province.findByName", Province.class)
				.setParameter("name", name)
				.getResultList();
		
		if(provinces.size() != 1) 
		{
			return null;
		}
		
		return provinces.get(0);
	}

	@Override
	public List<Province> getAllProvinces()
	{
		return em.createNamedQuery("Province.findAll", Province.class)
				.getResultList();
	}
}
