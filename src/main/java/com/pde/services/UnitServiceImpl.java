package com.pde.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.pde.models.Unit;

@Stateless(name = "UnitService")
public class UnitServiceImpl implements UnitService
{
	@Inject
	private EntityManager em;
	
	@Override
	public List<Unit> getAllUnits()
	{
		return em.createNamedQuery("Unit.findAll", Unit.class)
				.getResultList();
	}

	@Override
	public Unit getUnitByName(String name)
	{
		if(name == null)
		{
			return null;
		}
		
		List<Unit> units = em.createNamedQuery("Unit.findByName", Unit.class)
			.setParameter("name", name)
			.getResultList();
		
		if(units.size() > 1)
		{
			System.out.println("Duplicated units in database");
		}
		
		if(units.size() != 1)
		{
			return null;
		}
		
		return units.get(0);
	}
}
