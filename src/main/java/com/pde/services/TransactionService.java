package com.pde.services;

import java.util.Date;
import java.util.List;

import com.pde.models.Transaction;

public interface TransactionService
{
	public boolean createTransaction(Transaction transaction);
	
	public List<Transaction> getTransactionsBetweenDates(Date start, Date end);
	
	public List<Transaction> getTransactionsByStatus(Transaction.Status status);
	
	public Transaction getTransaction(long transactionId);
	
	public boolean updateTransaction(Transaction transaction);
}
