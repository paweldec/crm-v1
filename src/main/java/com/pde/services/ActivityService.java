package com.pde.services;

import java.util.List;

import com.pde.models.Activity;

public interface ActivityService
{
	public List<Activity> getAllActivities();
	
	public Activity getActivityByName(String name);
}
