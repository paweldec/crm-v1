package com.pde.services;

import java.util.List;

import com.pde.models.User;

public interface UserService
{
	public User login(String login, String password);
	
	public List<User> getAllUsers();
	
	public boolean createUser(User newUser);
	
	public boolean removeUser(Long userId);
	
	public User getUser(Long userId);
	
	public boolean updateUser(User user);
}
