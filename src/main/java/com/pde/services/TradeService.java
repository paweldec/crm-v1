package com.pde.services;

import java.util.List;

import com.pde.models.Trade;

public interface TradeService
{
	public List<Trade> getAllTrades();
	
	public Trade getTradeByName(String name);
}
