package com.pde.services;

import java.util.List;

import com.pde.models.City;

public interface CityService
{
	public List<String> autoComplete(String query);
	
	public City getCityByName(String name);
}
