package com.pde.services;

import java.util.List;

import com.pde.models.Unit;

public interface UnitService
{
	public List<Unit> getAllUnits();
	
	public Unit getUnitByName(String name);
}
