package com.pde.services;

import java.util.List;

import com.pde.models.Province;

public interface ProvinceService
{
	public Province getProvinceByName(String name);
	
	public List<Province> getAllProvinces();
}
