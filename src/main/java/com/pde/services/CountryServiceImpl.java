package com.pde.services;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.pde.models.Country;

@Stateless(name = "CountryService")
public class CountryServiceImpl implements CountryService
{
	@Inject
	private EntityManager em;
	
	@Override
	public Country getCountryByName(String name)
	{
		List<Country> countries = em.createNamedQuery("Country.findByName", Country.class)
				.setParameter("name", name)
				.getResultList();
		
		if(countries.size() > 1)
		{
			System.out.println("Duplicated countries in database");
		}
		
		if(countries.size() != 1) 
		{
			return null;
		}
		
		return countries.get(0);
	}

	@Override
	public List<String> autoComplete(String query)
	{
		List<Country> countries = em.createNamedQuery("Country.findByNameLike", Country.class)
				.setParameter("name", query.toUpperCase() + '%')
				.getResultList();
		
		List<String> countriesNames = new ArrayList<String>();
		
		for(Country country : countries)
		{
			countriesNames.add(country.getName());
		}
		
		return countriesNames;
	}
}
