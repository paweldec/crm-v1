package com.pde.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.pde.models.Contractor;
import com.pde.models.ScheduleEvent;
import com.pde.models.User;
import com.pde.models.Visibility;

@Stateless(name = "ScheduleService")
public class ScheduleServiceImpl implements ScheduleService
{
	@Inject
	private EntityManager em;
	
	@Override
	public List<ScheduleEvent> getUserScheduleEvents(User user)
	{
		return em.createNamedQuery("ScheduleEvent.findByUser", ScheduleEvent.class)
				.setParameter("user", user)
				.getResultList();
	}

	@Override
	public ScheduleEvent getScheduleEvent(String eventId)
	{
		List<ScheduleEvent> scheduleEvents = em.createNamedQuery("ScheduleEvent.findByEventId", ScheduleEvent.class)
				.setParameter("eventId", eventId)
				.getResultList();
		
		if(scheduleEvents.size() > 1)
		{
			System.out.println("Schedule event with the same id in database!");
		}
		
		if(scheduleEvents.size() != 1)
		{
			return null;
		}
		
		return scheduleEvents.get(0);
	}

	@Override
	public boolean update(ScheduleEvent scheduleEvent)
	{
		try
		{
			em.merge(scheduleEvent);
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
			return false;
		}
		
		return true;
	}

	@Override
	public boolean create(ScheduleEvent scheduleEvent)
	{
		if(scheduleEvent == null)
		{
			return false;
		}
		
		try
		{
			em.persist(scheduleEvent);
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
			return false;
		}
		
		return true;
	}

	@Override
	public boolean remove(long scheduleEventId)
	{
		ScheduleEvent scheduleEvent = em.find(ScheduleEvent.class, scheduleEventId);
		
		if(scheduleEvent == null)
		{
			return false;
		}
			
		em.remove(scheduleEvent);
		
		return true;
	}
}
