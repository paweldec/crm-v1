package com.pde.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.pde.models.Trade;

@Stateless(name = "TradeService")
public class TradeServiceImpl implements TradeService
{
	@Inject
	private EntityManager em;
	
	@Override
	public List<Trade> getAllTrades()
	{
		return em.createNamedQuery("Trade.findAll", Trade.class)
				.getResultList();
	}

	@Override
	public Trade getTradeByName(String name)
	{
		List<Trade> trades = em.createNamedQuery("Trade.findByName", Trade.class)
				.setParameter("name", name)
				.getResultList();
		
		if(trades.size() > 1)
		{
			System.out.println("Duplicated trades in database");
		}
		
		if(trades.size() != 1)
		{
			return null;
		}
		
		return trades.get(0);
	}

}
