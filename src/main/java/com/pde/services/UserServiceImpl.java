package com.pde.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.pde.models.User;
import com.pde.models.Visibility;

@Stateless(name = "UserService")
public class UserServiceImpl implements UserService
{
	@Inject
	private EntityManager em;

	@Override
	public User login(String login, String password)
	{
		List<User> users = em.createNamedQuery("User.findByLoginAndPassword", User.class)
				.setParameter("login", login)
				.setParameter("password", password)
				.getResultList();
		
		if(users.size() < 1)
		{
			return null;
		}
	
		return users.get(0);
	}

	@Override
	public List<User> getAllUsers()
	{
		return em.createNamedQuery("User.findAll", User.class).getResultList();
	}

	@Override
	public boolean createUser(User newUser)
	{
		if(newUser == null)
		{
			return false;
		}
		
		List<User> users = em.createNamedQuery("User.findByLogin", User.class)
					.setParameter("login", newUser.getLogin())
					.getResultList();
		
		if(users.size() != 0)
		{
			return false;
		}
		
		try
		{
			em.persist(newUser);
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
			return false;
		}
		
		return true;
	}

	@Override
	public boolean removeUser(Long userId)
	{
		User user = em.find(User.class, userId);
		
		if(user == null)
		{
			return false;
		}
		
		user.setVisibility(Visibility.HIDDEN);
		
		try
		{
			em.merge(user);
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
			return false;
		}
		
		return true;
	}

	@Override
	public User getUser(Long userId)
	{
		User user = em.find(User.class, userId);
		return user;
	}

	@Override
	public boolean updateUser(User user)
	{
		try
		{
			em.merge(user);
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
			return false;
		}
		
		return true;
	}

}
