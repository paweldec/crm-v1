package com.pde.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.pde.models.Activity;

@Stateless(name = "ActivityService")
public class ActivityServiceImpl implements ActivityService
{
	@Inject
	private EntityManager em;
	
	@Override
	public List<Activity> getAllActivities()
	{
		return em.createNamedQuery("Activity.findAll", Activity.class)
				.getResultList();
	}

	@Override
	public Activity getActivityByName(String name)
	{
		List<Activity> activities = em.createNamedQuery("Activity.findByName", Activity.class)
				.setParameter("name", name)
				.getResultList();
		
		if(activities.size() > 1)
		{
			System.out.println("Duplicated activities in database");
		}
		
		if(activities.size() != 1)
		{
			return null;
		}
		
		return activities.get(0);
	}

}
