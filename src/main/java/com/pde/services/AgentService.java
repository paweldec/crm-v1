package com.pde.services;

import java.util.List;

import com.pde.models.Agent;

public interface AgentService
{
	public List<Agent> getAllAgents();
	
	public boolean createAgent(Agent agent);
	
	public boolean removeAgent(Long agentId);
	
	public Agent getAgent(Long agentId);
	
	public boolean updateAgent(Agent agent);
}
