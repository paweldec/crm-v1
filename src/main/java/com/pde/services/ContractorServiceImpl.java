package com.pde.services;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.pde.models.Activity;
import com.pde.models.City;
import com.pde.models.Contractor;
import com.pde.models.Country;
import com.pde.models.Province;
import com.pde.models.Trade;
import com.pde.models.Visibility;

@Stateless(name = "ContractorService")
public class ContractorServiceImpl implements ContractorService
{
	@Inject
	private EntityManager em;
	
	@Override
	public List<Contractor> getAllContractors()
	{
		return em.createNamedQuery("Contractor.findAll", Contractor.class).getResultList();
	}

	@Override
	public boolean createContractor(Contractor contractor)
	{
		if(contractor == null) 
		{
			return false;
		}
		
		List<Contractor> contractors = em.createNamedQuery("Contractor.findByUnique", Contractor.class)
				.setParameter("name", contractor.getName())
				.setParameter("NIP", contractor.getNIP())
				.setParameter("REGON", contractor.getREGON())
				.setParameter("PESEL", contractor.getPESEL())
				.getResultList();
		
		if(contractors.size() != 0) 
		{
			return false;
		}
		
		try 
		{			
			////
			City city = contractor.getCity();
			
			if(city != null)
			{
				if(em.find(City.class, city.getId()) == null)
				{
					em.persist(city);
				}
				else
				{
					contractor.setCity(em.merge(city));
				}
			}
			
			Country country = contractor.getCountry();
			
			if(country != null)
			{
				if(em.find(Country.class, country.getId()) == null)
				{
					em.persist(country);
				}
				else
				{
					contractor.setCountry(em.merge(country));
				}
			}
			
			Province province = contractor.getProvince();
			
			if(province != null)
			{				
				if(em.find(Province.class, province.getId()) == null)
				{
					em.persist(province);
				}
				else
				{
					contractor.setProvince(em.merge(province));
				}
			}
			
			Trade trade = contractor.getTrade();
			
			if(trade != null)
			{
				if(em.find(Trade.class, trade.getId()) == null)
				{
					em.persist(trade);
				}
				else
				{
					contractor.setTrade(em.merge(trade));
				}
			}
			
			Activity activity = contractor.getActivity();
			
			if(activity != null)
			{
				if(em.find(Activity.class, activity.getId()) == null)
				{
					em.persist(activity);
				}
				else
				{
					contractor.setActivity(em.merge(activity));
				}
			}
			////
			em.persist(contractor);
		}
		catch(Exception e) 
		{
			System.out.println(e.getMessage());
			return false;
		}
		
		return true;
	}
	/*
	public <T> void saveOrUpdate(Class<T> _class)
	{
		_class.
	}
	*/
	@Override
	public boolean removeContractor(Long contractorId)
	{
		Contractor contractor = em.find(Contractor.class, contractorId);
		
		if(contractor == null)
		{
			return false;
		}
		
		contractor.setVisibility(Visibility.HIDDEN);	
		em.merge(contractor);
		
		return true;
	}

	@Override
	public Contractor getContractor(Long contractorId)
	{
		Contractor contractor = em.find(Contractor.class, contractorId);
		return contractor;
	}

	@Override
	public boolean updateContractor(Contractor contractor)
	{
		try
		{
			em.merge(contractor);
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
			return false;
		}
		
		return true;
	}
	
	@Override
	public List<String> autoComplete(String query)
	{
		List<Contractor> contractors = em.createNamedQuery("Contractor.findByNameLike", Contractor.class)
				.setParameter("name", query.toUpperCase() + '%')
				.getResultList();
		
		List<String> contractorsNames = new ArrayList<String>();
		
		for(Contractor contractor : contractors)
		{
			contractorsNames.add(contractor.getName());
		}
		
		return contractorsNames;
	}

	@Override
	public Contractor getContractorByName(String name)
	{
		List<Contractor> contractors = em.createNamedQuery("Contractor.findByName", Contractor.class)
				.setParameter("name", name)
				.getResultList();
		
		if(contractors.size() > 1)
		{
			System.out.println("Contracotrs with the same name in database!");
		}
		
		if(contractors.size() != 1)
		{
			return null;
		}
		
		return contractors.get(0);
	}
}
