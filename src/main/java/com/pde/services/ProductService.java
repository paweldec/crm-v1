package com.pde.services;

import java.util.List;

import com.pde.models.Product;

public interface ProductService
{
	public boolean createProduct(Product product);
	
	public List<Product> getAllProducts();
	
	public boolean removeProduct(long productId);
	
	public Product getProduct(long productId);
	
	public boolean updateProduct(Product product);
	
	public int getProductAmount(long productId);
}
